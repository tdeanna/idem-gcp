import pytest


@pytest.mark.asyncio
async def test_crypto_key_version_list(hub, ctx):
    parent = f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1/cryptoKeys/cicd-key-1"
    ret = await hub.exec.gcp.cloudkms.crypto_key_version.list(ctx, crypto_key=parent)
    assert ret["result"], ret["comment"]
    assert len(ret["ret"]) > 0
    assert ret["ret"][0]["resource_id"] == f"{parent}/cryptoKeyVersions/1"


@pytest.mark.asyncio
async def test_crypto_key_version_list_empty(hub, ctx):
    crypto_key = f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1/cryptoKeys/cicd-key-1000"
    ret = await hub.exec.gcp.cloudkms.crypto_key_version.list(
        ctx, crypto_key=crypto_key
    )
    assert not ret["result"], ret["comment"]
    assert len(ret["ret"]) == 0
    assert len(ret["comment"]) == 1 and ret["comment"][0].startswith("<HttpError 404 ")


@pytest.mark.asyncio
async def test_crypto_key_version_list_empty_unavailable_key(hub, ctx):
    resource_id = f"projects/{ctx.acct.project_id}/locations/us/keyRings/lazo-idem-gcp-test-1/cryptoKeys/key-1"
    ret = await hub.exec.gcp.cloudkms.crypto_key_version.list(
        ctx, crypto_key=resource_id
    )
    assert ret["result"], ret["comment"]
    assert len(ret["ret"]) == 0


@pytest.mark.asyncio
async def test_crypto_key_version_get(hub, ctx):
    resource_id = f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1/cryptoKeys/cicd-key-1/cryptoKeyVersions/1"
    ret = await hub.exec.gcp.cloudkms.crypto_key_version.get(
        ctx, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]["resource_id"] == resource_id


@pytest.mark.asyncio
async def test_crypto_key_version_get_empty(hub, ctx):
    resource_id = f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1/cryptoKeys/cicd-key-1/cryptoKeyVersions/1000"
    ret = await hub.exec.gcp.cloudkms.crypto_key_version.get(
        ctx, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert len(ret["comment"]) == 1 and ret["comment"][
        0
    ] == hub.tool.gcp.comment_utils.get_empty_comment(
        "gcp.cloudkms.crypto_key_version", resource_id
    ), ret[
        "comment"
    ]


@pytest.mark.asyncio
async def test_crypto_key_version_import(hub, ctx):
    parent = f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1/cryptoKeys/cicd-key-1"
    import_job = f"projects/{ctx.acct.project_id}/locations/us-east1/keyRings/cicd-idem-gcp-1/importJobs/cicd-import-job-1"
    algorithm = "GOOGLE_SYMMETRIC_ENCRYPTION"
    key_material = "rr5Y2UNi6+i3UQDrR8PO6s5ajAorN/SnHfZu+OCHx+w="
    f"{parent}/cryptoKeyVersions/4"
    import_key_ret = (
        await hub.tool.gcp.cloudkms.crypto_key_version_utils.get_import_key_if_eligible(
            ctx,
            key_material is not None,
            algorithm is not None,
            import_job,
            "DESTROYED",
        )
    )
    assert not import_key_ret["result"]
    assert (
        len(import_key_ret["comment"]) == 1
        and import_key_ret["comment"][0]
        == f"Import job {import_job} should be in state ACTIVE but current state is EXPIRED"
    )

    # TODO: Enable after cleanup task is solved
    # ret = await hub.exec.gcp.cloudkms.crypto_key_version["import"](
    #     ctx,
    #     parent=parent,
    #     import_job=import_job,
    #     import_job_pub_key=import_key_ret["ret"],
    #     algorithm=algorithm,
    #     key_material=key_material,
    #     crypto_key_version=crypto_key_version,
    # )
    # assert ret["result"], ret["comment"]
