from collections import ChainMap

import pytest

from tests.utils import generate_unique_name

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])

RESOURCE_TYPE = "iam.service_account_key"
GCP_RESOURCE_TYPE = f"gcp.{RESOURCE_TYPE}"

PRESENT_CREATE_STATE = {
    "name": "random-name",
    "private_key_type": "TYPE_PKCS12_FILE",
    "key_algorithm": "KEY_ALG_RSA_2048",
}
NAME = generate_unique_name("idem-account", 30)
SERVICE_ACCOUNT_STATE = {
    "name": NAME,
    "account_id": NAME,
    "description": "Created by automated gcp-idem IT for service accounts.",
    "display_name": "idem-gcp-it",
}


@pytest.fixture(scope="module")
async def service_account(hub, ctx, idem_cli, cleaner):
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, "iam.service_account", SERVICE_ACCOUNT_STATE
    )
    assert ret["result"], ret["comment"]
    yield ret["new_state"]
    cleaner(ret["new_state"], "iam.service_account")


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    ret = await hub.states.gcp.iam.service_account_key.describe(ctx)
    for resource_id in ret:
        assert "gcp.iam.service_account_key.present" in ret[resource_id]
        described_resource = ret[resource_id].get("gcp.iam.service_account_key.present")
        assert described_resource
        service_account_key = dict(ChainMap(*described_resource))
        assert service_account_key.get("resource_id") == resource_id


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_create")
def test_present_create(hub, idem_cli, tests_dir, __test, service_account, cleaner):
    key_state = {
        **PRESENT_CREATE_STATE,
        "service_account_id": service_account["resource_id"],
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, key_state, __test
    )

    assert ret["result"], ret["comment"]

    cleaner(ret["new_state"], "iam.service_account_key")

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=GCP_RESOURCE_TYPE,
                name=key_state["name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=GCP_RESOURCE_TYPE,
                name=ret["new_state"]["resource_id"],
            )
            in ret["comment"]
        )
