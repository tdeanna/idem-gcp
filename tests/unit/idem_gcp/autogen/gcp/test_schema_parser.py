import json

from cloudspec import CloudSpecParam

_SCHEMA_SIMPLE_PARAMS = {
    "resource_name": "BackendServiceLocalityLoadBalancingPolicyConfigCustomPolicy",
    "properties": {
        "data": {
            "type": "string",
        },
        "name": {"type": "integer"},
    },
}

_SCHEMA_AUTOSCALER = r"""{
"Autoscaler": {
        "type": "object",
        "id": "Autoscaler",
        "properties": {
            "statusDetails": {
              "type": "array",
              "description": "[Output Only] Human-readable details about the current state of the autoscaler. Read the documentation for Commonly returned status messages for examples of status messages you might encounter.",
              "items": {
                "$ref": "AutoscalerStatusDetails"
              }
            }
        }
    },
"AutoscalerStatusDetails": {
    "type": "object",
    "id": "AutoscalerStatusDetails",
    "properties": {
      "type": {
        "type": "string",
        "enum": [
          "ALL_INSTANCES_UNHEALTHY",
          "BACKEND_SERVICE_DOES_NOT_EXIST"
        ],
        "enumDescriptions": [
          "All instances in the instance group are unhealthy (not in RUNNING state).",
          "There is no backend service attached to the instance group."
        ],
        "description": "type property description"
      },
      "message": {
        "description": "The status message.",
        "type": "string"
      }
    }
  }
}"""

_EXPECTED_PARSED_SCHEMA_AUTOSCALER = {
    "AutoscalerStatusDetails": CloudSpecParam(
        **{
            "name": "unknown",
            "doc": "",
            "param_type": "{}",
            "required": False,
            "default": None,
            "target_type": "mapping",
            "member": {
                "name": "AutoscalerStatusDetails",
                "params": {
                    "type_": {
                        "doc": r"""type property description
Enum type. Allowed values:
"ALL_INSTANCES_UNHEALTHY" - All instances in the instance group are unhealthy (not in RUNNING state).
"BACKEND_SERVICE_DOES_NOT_EXIST" - There is no backend service attached to the instance group.""",
                        "param_type": "str",
                        "required": False,
                        "default": None,
                        "target_type": "mapping",
                        "member": None,
                        "target": "kwargs",
                    },
                    "message": {
                        "doc": "The status message.",
                        "param_type": "str",
                        "required": False,
                        "default": None,
                        "target_type": "mapping",
                        "member": None,
                        "target": "kwargs",
                    },
                },
            },
            "target": "kwargs",
        }
    ),
    "Autoscaler": CloudSpecParam(
        **{
            "name": "unknown",
            "doc": "",
            "param_type": "{}",
            "required": False,
            "default": None,
            "target_type": "mapping",
            "member": {
                "name": "Autoscaler",
                "params": {
                    "status_details": {
                        "doc": "[Output Only] Human-readable details about the current state of the autoscaler. Read the documentation for Commonly returned status messages for examples of status messages you might encounter.",
                        "param_type": "List[{}]",
                        "required": False,
                        "default": None,
                        "target_type": "mapping",
                        "member": {
                            "name": "AutoscalerStatusDetails",
                            "params": {
                                "type_": {
                                    "doc": r"""type property description
Enum type. Allowed values:
"ALL_INSTANCES_UNHEALTHY" - All instances in the instance group are unhealthy (not in RUNNING state).
"BACKEND_SERVICE_DOES_NOT_EXIST" - There is no backend service attached to the instance group.""",
                                    "param_type": "str",
                                    "required": False,
                                    "default": None,
                                    "target_type": "mapping",
                                    "member": None,
                                    "target": "kwargs",
                                },
                                "message": {
                                    "doc": "The status message.",
                                    "param_type": "str",
                                    "required": False,
                                    "default": None,
                                    "target_type": "mapping",
                                    "member": None,
                                    "target": "kwargs",
                                },
                            },
                        },
                        "target": "kwargs",
                    }
                },
            },
            "target": "kwargs",
        }
    ),
}

_SCHEMA_SIMPLE = f"""{{
    \"{_SCHEMA_SIMPLE_PARAMS["resource_name"]}\": {{
        "properties": {{
          "data": {{
            "type": \"{_SCHEMA_SIMPLE_PARAMS["properties"]["data"]["type"]}\"
          }},
          "name": {{
            "type": \"{_SCHEMA_SIMPLE_PARAMS["properties"]["name"]["type"]}\"
          }}
        }},
        "type": "object",
        "id": \"{_SCHEMA_SIMPLE_PARAMS["resource_name"]}\"
      }}
  }}"""

_SCHEMA_WITH_REF = """{
    "NetworksUpdatePeeringRequest": {
        "type": "object",
        "id": "NetworksUpdatePeeringRequest",
        "properties": {
          "networkPeering": {
            "$ref": "NetworkPeering",
            "description": "networkPeering property description"
          }
        }
    },
    "NetworkPeering": {
        "description": "A network peering attached to a network resource. The message includes the peering name, peer network, peering state, and a flag indicating whether Google Compute Engine should automatically create routes for the peering.",
        "type": "object",
        "id": "NetworkPeering",
        "properties": {
          "autoCreateRoutes": {
            "description": "This field will be deprecated soon. Use the exchange_subnet_routes field instead. Indicates whether full mesh connectivity is created and managed automatically between peered networks. Currently this field should always be true since Google Compute Engine will automatically create and manage subnetwork routes between two networks when peering state is ACTIVE.",
            "type": "boolean"
          },
          "peerMtu": {
            "type": "integer",
            "format": "int32",
            "description": "Maximum Transmission Unit in bytes."
          }
        }
    }
}"""

_EXPECTED_PARSED_SCHEMA_WITH_REF = {
    "NetworkPeering": CloudSpecParam(
        **{
            "name": "unknown",
            "doc": "A network peering attached to a network resource. The message includes the peering name, peer network, peering state, and a flag indicating whether Google Compute Engine should automatically create routes for the peering.",
            "param_type": "{}",
            "required": False,
            "default": None,
            "target_type": "mapping",
            "member": {
                "name": "NetworkPeering",
                "params": {
                    "auto_create_routes": {
                        "doc": "This field will be deprecated soon. Use the exchange_subnet_routes field instead. Indicates whether full mesh connectivity is created and managed automatically between peered networks. Currently this field should always be true since Google Compute Engine will automatically create and manage subnetwork routes between two networks when peering state is ACTIVE.",
                        "param_type": "bool",
                        "required": False,
                        "default": None,
                        "target_type": "mapping",
                        "member": None,
                        "target": "kwargs",
                    },
                    "peer_mtu": {
                        "doc": "Maximum Transmission Unit in bytes.",
                        "param_type": "int",
                        "required": False,
                        "default": None,
                        "target_type": "mapping",
                        "member": None,
                        "target": "kwargs",
                    },
                },
            },
            "target": "kwargs",
        }
    ),
    "NetworksUpdatePeeringRequest": CloudSpecParam(
        **{
            "name": "unknown",
            "doc": "",
            "param_type": "{}",
            "required": False,
            "default": None,
            "target_type": "mapping",
            "member": {
                "name": "NetworksUpdatePeeringRequest",
                "params": {
                    "network_peering": {
                        "required": False,
                        "target": "kwargs",
                        "target_type": "mapping",
                    }
                },
            },
            "target": "kwargs",
        }
    ),
}

_EXPECTED_MEMBER_WITH_REF = dict(**_EXPECTED_PARSED_SCHEMA_WITH_REF["NetworkPeering"])
_EXPECTED_MEMBER_WITH_REF["snaked"] = "network_peering"
_EXPECTED_MEMBER_WITH_REF[
    "doc"
] = "networkPeering property description\nNetworkPeering: A network peering attached to a network resource. The message includes the peering name, peer network, peering state, and a flag indicating whether Google Compute Engine should automatically create routes for the peering."
_EXPECTED_PARSED_SCHEMA_WITH_REF["NetworksUpdatePeeringRequest"]["member"]["params"][
    "network_peering"
] = _EXPECTED_MEMBER_WITH_REF


_SCHEMA_OBJECT_WITH_ADDITIONAL_PROPERTIES = r"""{
    "HealthStatus": {
        "properties": {
          "annotations": {
            "additionalProperties": {
              "type": "string"
            },
            "type": "object",
            "description": "Metadata defined as annotations for network endpoint."
          }
        },
        "id": "HealthStatus",
        "type": "object"
    }
}"""

_EXPECTED_PARSED_SCHEMA_OBJECT_WITH_ADDITIONAL_PROPERTIES = {
    "HealthStatus": CloudSpecParam(
        **{
            "name": "unknown",
            "doc": "",
            "param_type": "{}",
            "required": False,
            "default": None,
            "target_type": "mapping",
            "member": {
                "name": "HealthStatus",
                "params": {
                    "annotations": {
                        "doc": "Metadata defined as annotations for network endpoint.",
                        "param_type": "Dict[str, Any]",
                        "required": False,
                        "default": None,
                        "target_type": "mapping",
                        "member": None,
                        "target": "kwargs",
                    }
                },
            },
            "target": "kwargs",
        }
    ),
}

_SCHEMA_EMPTY_ENUM_DESCRIPTIONS = r"""{
"AutoscalerStatusDetails": {
    "type": "object",
    "id": "AutoscalerStatusDetails",
    "properties": {
      "type": {
        "type": "string",
        "enum": [
          "ALL_INSTANCES_UNHEALTHY",
          "BACKEND_SERVICE_DOES_NOT_EXIST"
        ],
        "enumDescriptions": [
          "",
          ""
        ]
      }
    }
  }
}"""

_EXPECTED_PARSED_SCHEMA_EMPTY_ENUM_DESCRIPTIONS = {
    "AutoscalerStatusDetails": CloudSpecParam(
        **{
            "name": "unknown",
            "doc": "",
            "param_type": "{}",
            "required": False,
            "default": None,
            "target_type": "mapping",
            "member": {
                "name": "AutoscalerStatusDetails",
                "params": {
                    "type_": {
                        "doc": """Enum type. Allowed values:
"ALL_INSTANCES_UNHEALTHY"
"BACKEND_SERVICE_DOES_NOT_EXIST\"""",
                        "param_type": "str",
                        "required": False,
                        "default": None,
                        "target_type": "mapping",
                        "member": None,
                        "target": "kwargs",
                    }
                },
            },
            "target": "kwargs",
        }
    )
}

_SCHEMA_LIST_OF_PRIMITIVES = r"""{
"TargetPool": {
      "description": "Represents a Target Pool resource.",
      "type": "object",
      "properties": {
        "healthChecks": {
          "type": "array",
          "description": "The URL of the HttpHealthCheck resource.",
          "items": {
            "type": "string"
          }
        }
      },
      "id": "TargetPool"
    }
}"""

_EXPECTED_PARSED_SCHEMA_LIST_OF_PRIMITIVES = {
    "TargetPool": CloudSpecParam(
        **{
            "name": "unknown",
            "doc": "Represents a Target Pool resource.",
            "param_type": "{}",
            "required": False,
            "default": None,
            "target_type": "mapping",
            "member": {
                "name": "TargetPool",
                "params": {
                    "health_checks": {
                        "doc": "The URL of the HttpHealthCheck resource.",
                        "param_type": "List[str]",
                        "required": False,
                        "default": None,
                        "target_type": "mapping",
                        "member": None,
                        "target": "kwargs",
                    }
                },
            },
            "target": "kwargs",
        }
    )
}


def test_parse_json(autogenhub):
    json_dict = json.loads(_SCHEMA_SIMPLE)
    parsed_schemas = autogenhub.autogen.gcp.schema_parser.parse_json(json_dict)
    assert len(parsed_schemas) == len(json_dict)
    for resource_name, resource_schema in json_dict.items():
        parsed_schema = parsed_schemas.get(resource_name)
        assert parsed_schema is not None
        assert parsed_schema.id_ == resource_schema["id"]
        resource_schema_properties = resource_schema["properties"]
        assert len(parsed_schema.properties) == len(resource_schema_properties)
        for property_name, property_value in resource_schema_properties.items():
            parsed_value = parsed_schema.properties.get(property_name)
            assert parsed_value is not None
            assert parsed_value.type_ == property_value["type"]


def test_parse_schemas(autogenhub):
    json_dict = json.loads(_SCHEMA_SIMPLE)
    parsed_schemas = autogenhub.autogen.gcp.schema_parser.parse_json(json_dict)
    cloudspec_params = autogenhub.autogen.gcp.schema_parser.parse_schemas(
        parsed_schemas
    )
    assert len(cloudspec_params) == len(parsed_schemas)
    resource_name = _SCHEMA_SIMPLE_PARAMS["resource_name"]
    cloudspec_param = cloudspec_params.get(resource_name)
    assert isinstance(cloudspec_param, CloudSpecParam)
    assert cloudspec_param == {
        "doc": "",
        "param_type": "{}",
        "required": False,
        "default": None,
        "target": "kwargs",
        "target_type": "mapping",
        "snaked": "unknown",
        "member": {
            "name": _SCHEMA_SIMPLE_PARAMS["resource_name"],
            "params": {
                "data": {
                    "doc": "",
                    "param_type": autogenhub.autogen.gcp.schema_parser.convert_primitive_type(
                        _SCHEMA_SIMPLE_PARAMS["properties"]["data"]["type"]
                    ),
                    "required": False,
                    "default": None,
                    "member": None,
                    "target": "kwargs",
                    "target_type": "mapping",
                    "snaked": "data",
                },
                "name": {
                    "doc": "",
                    "param_type": autogenhub.autogen.gcp.schema_parser.convert_primitive_type(
                        _SCHEMA_SIMPLE_PARAMS["properties"]["name"]["type"]
                    ),
                    "required": False,
                    "default": None,
                    "member": None,
                    "target": "kwargs",
                    "target_type": "mapping",
                    "snaked": "name",
                },
            },
        },
    }


def test_parse_schema_autoscaler(autogenhub):
    json_dict = json.loads(_SCHEMA_AUTOSCALER)
    parsed_schemas = autogenhub.autogen.gcp.schema_parser.parse_json(json_dict)
    cloudspec_params = autogenhub.autogen.gcp.schema_parser.parse_schemas(
        parsed_schemas
    )
    assert cloudspec_params == _EXPECTED_PARSED_SCHEMA_AUTOSCALER


def test_parse_schema_with_ref(autogenhub):
    json_dict = json.loads(_SCHEMA_WITH_REF)
    parsed_schemas = autogenhub.autogen.gcp.schema_parser.parse_json(json_dict)
    cloudspec_params = autogenhub.autogen.gcp.schema_parser.parse_schemas(
        parsed_schemas
    )
    assert cloudspec_params == _EXPECTED_PARSED_SCHEMA_WITH_REF


def test_parse_schema_with_additional_properties(autogenhub):
    json_dict = json.loads(_SCHEMA_OBJECT_WITH_ADDITIONAL_PROPERTIES)
    parsed_schemas = autogenhub.autogen.gcp.schema_parser.parse_json(json_dict)
    cloudspec_params = autogenhub.autogen.gcp.schema_parser.parse_schemas(
        parsed_schemas
    )
    assert cloudspec_params == _EXPECTED_PARSED_SCHEMA_OBJECT_WITH_ADDITIONAL_PROPERTIES


def test_parse_schema_with_empty_enum_descriptions(autogenhub):
    json_dict = json.loads(_SCHEMA_EMPTY_ENUM_DESCRIPTIONS)
    parsed_schemas = autogenhub.autogen.gcp.schema_parser.parse_json(json_dict)
    cloudspec_params = autogenhub.autogen.gcp.schema_parser.parse_schemas(
        parsed_schemas
    )
    assert cloudspec_params == _EXPECTED_PARSED_SCHEMA_EMPTY_ENUM_DESCRIPTIONS


def test_parse_schema_with_list_of_primitives(autogenhub):
    json_dict = json.loads(_SCHEMA_LIST_OF_PRIMITIVES)
    parsed_schemas = autogenhub.autogen.gcp.schema_parser.parse_json(json_dict)
    cloudspec_params = autogenhub.autogen.gcp.schema_parser.parse_schemas(
        parsed_schemas
    )
    assert cloudspec_params == _EXPECTED_PARSED_SCHEMA_LIST_OF_PRIMITIVES
