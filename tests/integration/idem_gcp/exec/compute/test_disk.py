from typing import Any
from typing import Dict
from typing import List

import pytest

from tests.utils import generate_unique_name

PARAMETER = {
    "health_check_1_name": generate_unique_name("idem-test-health-check"),
    "disk_name": generate_unique_name("idem-test-disk"),
    "snapshot_name": generate_unique_name("idem-test-snapshot"),
}


@pytest.fixture(scope="module")
async def gcp_disks(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.compute.disk.list(ctx, zone="us-central1-a")
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.fixture(scope="module")
async def test_disk(hub, ctx, idem_cli, tests_dir, cleaner) -> Dict[str, Any]:
    global PARAMETER

    if "disk_resource" in PARAMETER:
        yield PARAMETER["disk_resource"]

    path_to_sls = tests_dir / "sls" / "compute" / "default_disk_present.sls"
    with open(path_to_sls) as f:
        sls_str = f.read()
        sls_str = sls_str.format(
            **{
                "name": PARAMETER["disk_name"],
                "size_gb": "1",
                "resource_policies": [],
                "labels": {},
                "label_fingerprint": "",
            }
        )
        ret = hub.tool.utils.call_present_from_sls(idem_cli, sls_str)
        assert ret["result"], ret["comment"]

        disk = ret["new_state"]
        PARAMETER["disk_resource"] = disk
        cleaner(disk, "compute.disk")

        yield disk


@pytest.mark.asyncio
async def test_list_project_zone_filter(hub, ctx, gcp_disks):
    if len(gcp_disks) > 0:
        disk = gcp_disks[0]

        ret = await hub.exec.gcp.compute.disk.list(
            ctx,
            zone="us-central1-a",
            filter_=f"name eq {disk['name']}",
        )

        assert ret["ret"]
        assert ret["result"]
        assert len(ret["ret"]) == 1


@pytest.mark.asyncio
async def test_get_project_zone_name(hub, ctx, gcp_disks):
    if len(gcp_disks) > 0:
        disk = gcp_disks[0]

        ret = await hub.exec.gcp.compute.disk.get(
            ctx,
            zone="us-central1-a",
            name=f"{disk['name']}",
        )

        assert ret["result"]
        assert ret["ret"]
        assert not ret["comment"]

        returned_disk = ret["ret"]
        assert disk["name"] == returned_disk["name"]
        assert disk.get("resource_id") == returned_disk.get("resource_id")
        assert disk.get("name") in returned_disk.get("name")
        assert disk.get("zone") in returned_disk.get("zone")


@pytest.mark.asyncio
async def test_get_resource_id(hub, ctx, gcp_disks):
    if len(gcp_disks) > 0:
        disk = gcp_disks[0]

        ret = await hub.exec.gcp.compute.disk.get(
            ctx,
            resource_id=disk["resource_id"],
        )

        assert ret["result"]
        assert ret["ret"]
        assert not ret["comment"]

        returned_disk = ret["ret"]
        assert disk["name"] == returned_disk["name"]
        assert disk.get("resource_id") == returned_disk.get("resource_id")
        assert disk.get("name") in returned_disk.get("name")
        assert disk.get("zone") in returned_disk.get("zone")


@pytest.mark.asyncio
async def test_get_invalid_name(hub, ctx, gcp_disks):
    if len(gcp_disks) > 0:
        disk = gcp_disks[0]
        ret = await hub.exec.gcp.compute.disk.get(
            ctx,
            zone=disk.get("zone"),
            name="invalid-name",
        )
        assert ret["result"]
        assert not ret["ret"]
        assert "Get compute.disk 'None' result is empty" in ret["comment"]


@pytest.mark.asyncio
async def test_get_invalid_resource_id(hub, ctx):
    invalid_resource_id = "/projects/invalid/zone/invalid/disks/invalid-name"
    ret = await hub.exec.gcp.compute.disk.get(
        ctx,
        resource_id=invalid_resource_id,
    )
    assert not ret["result"], ret["comment"]
    assert not ret["ret"], ret["comment"]


@pytest.mark.asyncio
async def test_create_snapshot(hub, ctx, test_disk, cleaner):
    global PARAMETER

    disk = PARAMETER["disk_resource"]
    snapshot_name = PARAMETER["snapshot_name"]
    ret = await hub.exec.gcp.compute.disk.create_snapshot(
        ctx,
        resource_id=disk.get("resource_id"),
        name=snapshot_name,
        storage_locations=["us-central1"],
    )
    assert ret["result"]
    assert ret["ret"], ret["comment"]

    ret = await hub.exec.gcp.compute.snapshot.get(
        ctx,
        project=hub.tool.gcp.utils.get_project_from_account(ctx, None),
        name=snapshot_name,
    )
    assert ret["result"]
    assert ret["ret"], ret["comment"]

    PARAMETER["snapshot_resource"] = ret["ret"]
    cleaner(ret["ret"], "compute.snapshot")


@pytest.mark.asyncio
async def test_create_from_snapshot(hub, ctx, cleaner):
    global PARAMETER

    snapshot = PARAMETER["snapshot_resource"]
    disk = PARAMETER["disk_resource"]
    restored_disk_name = generate_unique_name("idem-test-disk")

    ret = await hub.exec.gcp.compute.disk.create_from_snapshot(
        ctx,
        source_snapshot=snapshot.get("resource_id"),
        name=restored_disk_name,
        project=hub.tool.gcp.utils.get_project_from_account(ctx, None),
        zone=disk.get("zone"),
        size_gb=disk.get("size_gb"),
        type_=disk.get("type_"),
    )
    assert ret["result"]
    assert ret["ret"], ret["comment"]

    restored_disk = ret["ret"]
    assert restored_disk
    assert int(restored_disk.get("size_gb")) >= int(disk.get("size_gb"))
    PARAMETER["restored_disk_name"] = restored_disk_name
    PARAMETER["restored_disk_resource"] = restored_disk
    cleaner(restored_disk, "compute.disk")
