from idem_gcp.tool.gcp.schema.results_collector import ResultsCollector


def test_has_breaking_finding_exists():
    collector = ResultsCollector()
    collector.add_breaking(schema_path="some path", description="description 1")
    collector.add_non_breaking(schema_path="other path", description="description 2")
    assert collector.has_breaking_finding() is True


def test_has_breaking_finding_empty():
    collector = ResultsCollector()
    assert collector.has_breaking_finding() is False


def test_has_breaking_finding_non_breaking_only():
    collector = ResultsCollector()
    collector.add_non_breaking(schema_path="some path", description="description 1")
    collector.add_non_breaking(schema_path="other path", description="description 2")
    assert collector.has_breaking_finding() is False
