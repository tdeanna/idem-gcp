import pytest

from tests.utils import generate_unique_name

PARAMETER = {
    "forwarding_rule_name": generate_unique_name("idem-test-forwarding-rule"),
    "region": "asia-east1",
}

RESOURCE_TYPE_FORWARDING_RULE = "compute.forwarding_rule"

TARGET_02 = "projects/tango-gcp/regions/asia-east1/targetPools/ventsy-tcp-pooled-02"


FORWARDING_RULE_SPEC = """
{forwarding_rule_name}:
    gcp.compute.forwarding_rule.present:
      - name: {forwarding_rule_name}
      - description: created via idem
      - region: asia-east1
      - ip_protocol: TCP
      - port_range: 80-80
      - target: projects/tango-gcp/regions/asia-east1/targetPools/ventsy-tcp-pooled-01
      - load_balancing_scheme: EXTERNAL
      - network_tier: STANDARD
"""


@pytest.fixture(scope="module")
def forwarding_rule_1(hub, idem_cli):
    # Create forwarding rule
    present_state_sls = FORWARDING_RULE_SPEC.format(**PARAMETER)

    forwarding_rule_ret = hub.tool.utils.call_present_from_sls(
        idem_cli,
        present_state_sls,
    )

    assert forwarding_rule_ret["result"], forwarding_rule_ret["comment"]
    resource_id = forwarding_rule_ret["new_state"]["resource_id"]
    PARAMETER["forwarding_rule_resource_id"] = resource_id

    try:
        yield forwarding_rule_ret["new_state"]
    finally:
        # Delete forwarding rule
        ret = hub.tool.utils.call_absent(
            idem_cli,
            RESOURCE_TYPE_FORWARDING_RULE,
            PARAMETER["forwarding_rule_name"],
            PARAMETER["forwarding_rule_resource_id"],
        )
        # if delete fail do not fail the test, log as warning
        if not ret["result"]:
            hub.log.warning(
                f"Could not cleanup forwarding rule {PARAMETER['forwarding_rule_resource_id']}",
                ret["comment"],
            )


@pytest.mark.asyncio
async def test_get_missing_name_arg(hub, ctx):
    ret = await hub.exec.gcp.compute.forwarding_rule.get(ctx)
    assert not ret["result"], ret["comment"]
    assert not ret["ret"]
    assert ret["comment"]
    assert 'Missing required parameter "forwardingRule"' in ret["comment"]


@pytest.mark.asyncio
async def test_get_by_name(hub, ctx, forwarding_rule_1):
    ret = await hub.exec.gcp.compute.forwarding_rule.get(
        ctx, name=forwarding_rule_1["name"], region=PARAMETER["region"]
    )
    assert ret["result"], ret["comment"]
    assert not ret["comment"]
    assert forwarding_rule_1["resource_id"] == ret["ret"].get("resource_id", None)
    assert forwarding_rule_1["name"] in ret["ret"].get("name", None)


@pytest.mark.asyncio
async def test_get_by_resource_id(hub, ctx, forwarding_rule_1):
    ret = await hub.exec.gcp.compute.forwarding_rule.get(
        ctx, resource_id=forwarding_rule_1["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"], ret["comment"]
    assert not ret["comment"]
    assert forwarding_rule_1["resource_id"] == ret["ret"].get("resource_id", None)
    assert forwarding_rule_1["name"] == ret["ret"].get("name", None)


@pytest.mark.asyncio
async def test_get_invalid_name(hub, ctx):
    invalid_name = "non-existing-rule"
    ret = await hub.exec.gcp.compute.forwarding_rule.get(
        ctx,
        name=invalid_name,
        region=PARAMETER["region"],
    )
    assert ret["result"], ret["comment"]
    assert not ret["ret"]
    assert any("result is empty" in c for c in ret["comment"])


@pytest.mark.asyncio
async def test_get_invalid_resource_id(hub, ctx):
    invalid_resource_id = f"/projects/{ctx.acct.project_id}/regions/{PARAMETER['region']}/forwardingRules/invalid-rule"
    ret = await hub.exec.gcp.compute.forwarding_rule.get(
        ctx, resource_id=invalid_resource_id
    )
    assert ret["result"], ret["comment"]
    assert not ret["ret"]
    assert any("result is empty" in c for c in ret["comment"])


@pytest.mark.asyncio
async def test_list_invalid_project(hub, ctx):
    ret = await hub.exec.gcp.compute.forwarding_rule.list(
        ctx, project="invalid-project"
    )
    assert not ret["result"], ret["comment"]
    assert not ret["ret"]
    assert ret["comment"]


@pytest.mark.asyncio
async def test_list_for_project(hub, ctx, forwarding_rule_1):
    ret = await hub.exec.gcp.compute.forwarding_rule.list(ctx)
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert not ret["comment"]
    assert len(ret["ret"]) > 0


@pytest.mark.asyncio
async def test_list_filter(hub, ctx, forwarding_rule_1):
    ret = await hub.exec.gcp.compute.forwarding_rule.list(
        ctx, filter_=f"name={forwarding_rule_1['name']}"
    )
    assert ret["result"], ret["comment"]
    assert not ret["comment"]
    assert ret["ret"]
    assert len(ret["ret"]) == 1
    assert forwarding_rule_1["name"] == ret["ret"][0].get("name", None)
    assert forwarding_rule_1["resource_id"] == ret["ret"][0].get("resource_id", None)


@pytest.mark.asyncio
async def test_list_invalid_filter(hub, ctx):
    ret = await hub.exec.gcp.compute.forwarding_rule.list(
        ctx, filter_="name eq unknown"
    )
    assert ret["result"], ret["comment"]
    assert not ret["ret"]
    assert len(ret["ret"]) == 0
    assert not ret["comment"]


@pytest.mark.asyncio
async def test_set_target(hub, ctx, forwarding_rule_1):
    ret = await hub.exec.gcp.compute.forwarding_rule.set_target(
        ctx, resource_id=PARAMETER["forwarding_rule_resource_id"], target=TARGET_02
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert ret["ret"].get("target") == TARGET_02


@pytest.mark.asyncio
async def test_set_invalid_target(hub, ctx, forwarding_rule_1):
    ret = await hub.exec.gcp.compute.forwarding_rule.set_target(
        ctx, resource_id=PARAMETER["forwarding_rule_resource_id"], target="dummy"
    )
    assert not ret["result"], ret["comment"]
    assert not ret["ret"]
    assert any("Invalid value for field 'target.target'" in c for c in ret["comment"])


@pytest.mark.asyncio
async def test_set_labels(hub, ctx, forwarding_rule_1):
    labels = {
        "key1": "val1",
        "key2": "val2",
    }

    ret = await hub.exec.gcp.compute.forwarding_rule.set_labels(
        ctx,
        resource_id=PARAMETER["forwarding_rule_resource_id"],
        region=PARAMETER["region"],
        labels=labels,
        label_fingerprint=forwarding_rule_1.get("label_fingerprint"),
    )

    assert ret["result"], ret["comment"]
    assert ret["ret"]
    assert not ret["comment"]

    assert ret["ret"].get("labels") == labels
