from typing import Any
from typing import Dict
from typing import List

import pytest


@pytest.fixture(scope="module")
async def gcp_firewalls(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.compute.firewall.list(ctx)
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.mark.asyncio
async def test_list_project_filter(hub, ctx, gcp_firewalls):
    if len(gcp_firewalls) > 0:
        firewall = gcp_firewalls[0]

        ret = await hub.exec.gcp.compute.firewall.list(
            ctx,
            filter_=f"name eq {firewall['name']}",
        )

        assert ret["ret"], ret["result"]
        assert len(ret["ret"]) == 1


@pytest.mark.asyncio
async def test_get_project_zone_name(hub, ctx, gcp_firewalls):
    if len(gcp_firewalls) > 0:
        firewall = gcp_firewalls[0]

        ret = await hub.exec.gcp.compute.firewall.get(
            ctx,
            name=f"{firewall['name']}",
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_firewall = ret["ret"]
        assert firewall["name"] == returned_firewall["name"]
        assert firewall.get("resource_id") == returned_firewall.get("resource_id")
        assert firewall.get("name") in returned_firewall.get("name")


@pytest.mark.asyncio
async def test_get_resource_id(hub, ctx, gcp_firewalls):
    if len(gcp_firewalls) > 0:
        firewall = gcp_firewalls[0]

        ret = await hub.exec.gcp.compute.firewall.get(
            ctx,
            resource_id=firewall["resource_id"],
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_firewall = ret["ret"]
        assert firewall["name"] == returned_firewall["name"]
        assert firewall.get("resource_id") == returned_firewall.get("resource_id")
        assert firewall.get("name") in returned_firewall.get("name")


@pytest.mark.asyncio
async def test_get_invalid_name(hub, ctx):
    ret = await hub.exec.gcp.compute.firewall.get(
        ctx,
        name="invalid-name",
    )
    assert ret["result"], not ret["ret"]
    assert "Get compute.firewall 'None' result is empty" in ret["comment"]


@pytest.mark.asyncio
async def test_get_invalid_resource_id(hub, ctx):
    invalid_resource_id = "/projects/invalid/global/firewalls/invalid-name"
    ret = await hub.exec.gcp.compute.firewall.get(
        ctx,
        resource_id=invalid_resource_id,
    )
    assert not ret["result"], not ret["ret"]
    assert ret["comment"]
