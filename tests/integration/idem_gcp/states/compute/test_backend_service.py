import copy
from collections import ChainMap

import pytest

from tests.utils import generate_unique_name

RESOURCE_TYPE_BACKEND_SERVICE = "compute.backend_service"
RESOURCE_TYPE_HEALTH_CHECK = "compute.health_check"

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": generate_unique_name("idem-test-backends"),
    "description": "Created by IDEM GCP Plugin integration tests.",
    "region": "us-central1",
    "project": "tango-gcp",
    "health_check_name": generate_unique_name("idem-test-backends"),
}

BACKEND_SERVICE_PRESENT = {
    "name": PARAMETER["name"],
    "project": PARAMETER["project"],
    "region": PARAMETER["region"],
    "description": PARAMETER["description"],
    "load_balancing_scheme": "INTERNAL_MANAGED",
    "timeout_sec": 60,
    "port": 80,
    "protocol": "HTTP",
    "port_name": "http",
    "session_affinity": "NONE",
    "affinity_cookie_ttl_sec": 10,
    "connection_draining": {"draining_timeout_sec": 300},
    "locality_lb_policy": "ROUND_ROBIN",
}

# TODO add backend services once we can create them
BACKEND_SERVICE_UPDATE = {
    "description": "Created by IDEM GCP Plugin integration tests. Updated description.",
    "timeout_sec": 30,
    "affinity_cookie_ttl_sec": 60,
    "session_affinity": "CLIENT_IP",
    "locality_lb_policy": "LEAST_REQUEST",
}

HEALTH_CHECK_SPEC = """
{health_check_name}:
  gcp.compute.health_check.present:
  - name: {health_check_name}
  - type_: TCP
  - tcp_health_check:
      port: 80
"""

RESOURCE_ID = f"projects/{PARAMETER['project']}/regions/{PARAMETER['region']}/backendServices/{PARAMETER['name']}"

# Data format { "the_resource_id": { "name": "name", "resource_type": "type" } }
RESOURCES_TO_DELETE = {}


@pytest.fixture(scope="module")
def health_check(hub, idem_cli):
    # Create health check
    present_state_sls = HEALTH_CHECK_SPEC.format(
        **{
            "health_check_name": PARAMETER["health_check_name"],
        }
    )

    health_check_ret = hub.tool.utils.call_present_from_sls(
        idem_cli,
        present_state_sls,
    )

    assert health_check_ret["result"], health_check_ret["comment"]
    resource_id = health_check_ret["new_state"]["resource_id"]
    PARAMETER["health_check_resource_id"] = resource_id

    # Cleaned up later, since it needs to be deleted after all backend services that use it
    yield health_check_ret["new_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_create")
def test_backend_service_present(hub, idem_cli, tests_dir, __test, health_check):
    global BACKEND_SERVICE_PRESENT
    BACKEND_SERVICE_PRESENT = {
        **BACKEND_SERVICE_PRESENT,
        "health_checks": [health_check["resource_id"]],
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_BACKEND_SERVICE, BACKEND_SERVICE_PRESENT, __test
    )
    assert ret["result"], ret["comment"]

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BACKEND_SERVICE}",
                name=PARAMETER["name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BACKEND_SERVICE}",
                name=PARAMETER["name"],
            )
            in ret["comment"]
        )
        RESOURCES_TO_DELETE[ret["new_state"]["resource_id"]] = {
            "name": PARAMETER["name"],
            "resource_type": RESOURCE_TYPE_BACKEND_SERVICE,
        }
    expected_state = {"resource_id": RESOURCE_ID, **BACKEND_SERVICE_PRESENT}
    changes = hub.tool.gcp.utils.compare_states(
        expected_state, ret["new_state"], RESOURCE_TYPE_BACKEND_SERVICE
    )
    assert not bool(changes), changes


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="backend_service_update", depends=["present_create"])
def test_backend_service_update(hub, idem_cli, tests_dir, __test):
    update_state = copy.deepcopy(BACKEND_SERVICE_PRESENT)
    expected_old_state = {
        "resource_id": RESOURCE_ID,
        **BACKEND_SERVICE_PRESENT,
    }

    update_state.update(BACKEND_SERVICE_UPDATE)

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_BACKEND_SERVICE, update_state, __test
    )
    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BACKEND_SERVICE}",
                name=PARAMETER["name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BACKEND_SERVICE}",
                name=PARAMETER["name"],
            )
            in ret["comment"]
        )
    expected_new_state = {"resource_id": RESOURCE_ID, **update_state}
    expected_new_state_result = hub.tool.gcp.utils.compare_states(
        expected_new_state, ret["new_state"], RESOURCE_TYPE_BACKEND_SERVICE
    )
    expected_old_state_result = hub.tool.gcp.utils.compare_states(
        expected_old_state, ret["old_state"], RESOURCE_TYPE_BACKEND_SERVICE
    )
    assert not bool(expected_new_state_result), expected_new_state_result
    assert not bool(expected_old_state_result), expected_old_state_result


@pytest.mark.asyncio
@pytest.mark.dependency(
    name="backend_service_describe", depends=["backend_service_update"]
)
async def test_backend_service_describe(hub, ctx):
    describe_ret = await hub.states.gcp.compute.backend_service.describe(ctx)
    assert RESOURCE_ID in describe_ret
    assert f"gcp.{RESOURCE_TYPE_BACKEND_SERVICE}.present" in describe_ret[RESOURCE_ID]
    described_resource = describe_ret[RESOURCE_ID].get(
        f"gcp.{RESOURCE_TYPE_BACKEND_SERVICE}.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))

    assert PARAMETER["name"] == described_resource_map["name"]
    assert RESOURCE_ID == described_resource_map["resource_id"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent", depends=["backend_service_describe"])
def test_backend_service_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_BACKEND_SERVICE,
        PARAMETER["name"],
        RESOURCE_ID,
        test=__test,
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BACKEND_SERVICE}",
                name=PARAMETER["name"],
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BACKEND_SERVICE}",
                name=PARAMETER["name"],
            )
            in ret["comment"]
        )
        RESOURCES_TO_DELETE.pop(RESOURCE_ID, None)
    assert not ret.get("new_state")


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
def test_backend_service_already_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_BACKEND_SERVICE,
        PARAMETER["name"],
        RESOURCE_ID,
        test=__test,
    )

    assert [
        hub.tool.gcp.comment_utils.already_absent_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_BACKEND_SERVICE}", name=PARAMETER["name"]
        )
    ] == ret["comment"]
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
    assert not ret.get("old_state")


@pytest.fixture(autouse=True, scope="module")
async def test_cleanup(hub, ctx, idem_cli):
    yield
    # Do cleanup
    for resource_id, resource_data in RESOURCES_TO_DELETE.items():
        ret = hub.tool.utils.call_absent(
            idem_cli,
            resource_data["resource_type"],
            resource_data["name"],
            resource_id,
        )
        if not ret["result"]:
            hub.log.debug(f"Error deleting resource {resource_id}: {ret['comment']}")

    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_HEALTH_CHECK,
        PARAMETER["health_check_name"],
        PARAMETER["health_check_resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("new_state")
