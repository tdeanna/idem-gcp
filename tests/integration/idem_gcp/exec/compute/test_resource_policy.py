from typing import Any
from typing import Dict
from typing import List

import pytest


@pytest.fixture(scope="module")
async def gcp_resource_policies(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.compute.resource_policy.list(ctx, region="us-central1")
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.mark.asyncio
async def test_list_project_region_filter(hub, ctx, gcp_resource_policies):
    if len(gcp_resource_policies) > 0:
        resource_policy = gcp_resource_policies[0]

        ret = await hub.exec.gcp.compute.resource_policy.list(
            ctx,
            region="us-central1",
            filter_=f"name eq {resource_policy['name']}",
        )

        assert ret["ret"], ret["result"]
        assert len(ret["ret"]) == 1


@pytest.mark.asyncio
async def test_get_project_region_name(hub, ctx, gcp_resource_policies):
    if len(gcp_resource_policies) > 0:
        resource_policy = gcp_resource_policies[0]

        ret = await hub.exec.gcp.compute.resource_policy.get(
            ctx,
            region="us-central1",
            name=f"{resource_policy['name']}",
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_resource_policy = ret["ret"]
        assert resource_policy["name"] == returned_resource_policy["name"]
        assert resource_policy.get("resource_id") == returned_resource_policy.get(
            "resource_id"
        )
        assert resource_policy.get("name") in returned_resource_policy.get("name")
        assert resource_policy.get("region") in returned_resource_policy.get("region")


@pytest.mark.asyncio
async def test_get_resource_id(hub, ctx, gcp_resource_policies):
    if len(gcp_resource_policies) > 0:
        resource_policy = gcp_resource_policies[0]

        ret = await hub.exec.gcp.compute.resource_policy.get(
            ctx,
            resource_id=resource_policy["resource_id"],
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_resource_policy = ret["ret"]
        assert resource_policy["name"] == returned_resource_policy["name"]
        assert resource_policy.get("resource_id") == returned_resource_policy.get(
            "resource_id"
        )
        assert resource_policy.get("name") in returned_resource_policy.get("name")
        assert resource_policy.get("region") in returned_resource_policy.get("region")


@pytest.mark.asyncio
async def test_get_invalid_name(hub, ctx, gcp_resource_policies):
    if len(gcp_resource_policies) > 0:
        resource_policy = gcp_resource_policies[0]
        ret = await hub.exec.gcp.compute.resource_policy.get(
            ctx,
            region=resource_policy.get("region"),
            name="invalid-name",
        )
        assert ret["result"], not ret["ret"]
        assert "Get compute.resource_policy 'None' result is empty" in ret["comment"]


@pytest.mark.asyncio
async def test_get_invalid_resource_id(hub, ctx):
    resource_id = "/projects/invalid/region/invalid/resourcePolicies/invalid-name"
    invalid_resource_id = resource_id
    with pytest.raises(ValueError) as e_info:
        ret = await hub.exec.gcp.compute.resource_policy.get(
            ctx,
            resource_id=invalid_resource_id,
        )
    resource_type = "compute.resource_policy"
    resource_paths = hub.tool.gcp.resource_prop_utils.get_resource_paths(resource_type)
    assert str(
        e_info.value
    ) == hub.tool.gcp.comment_utils.ill_formed_resource_id_comment(
        resource_type, resource_id, resource_paths
    )
