from typing import Any
from typing import Dict
from typing import List

import pytest


@pytest.fixture(scope="module")
async def gcp_target_pools(hub, ctx) -> Dict[str, Any]:
    ret = await hub.exec.gcp.compute.target_pool.list(ctx)
    assert ret["result"]
    assert isinstance(ret["ret"], List)
    yield ret["ret"]


@pytest.mark.asyncio
async def test_list_project_filter(hub, ctx, gcp_target_pools):
    if len(gcp_target_pools) > 0:
        target_pool = gcp_target_pools[0]

        ret = await hub.exec.gcp.compute.target_pool.list(
            ctx,
            filter_=f"name eq {target_pool['name']}",
        )

        returned_target_pool = ret["ret"]
        assert ret["ret"], ret["result"]
        assert target_pool.get("name") == returned_target_pool[0].get("name")
        assert len(ret["ret"]) == 1


@pytest.mark.asyncio
async def test_get_project_name(hub, ctx, gcp_target_pools):
    if len(gcp_target_pools) > 0:
        target_pool = gcp_target_pools[0]

        ret = await hub.exec.gcp.compute.target_pool.get(
            ctx, name=target_pool["name"], region=target_pool["region"]
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_target_pool = ret["ret"]
        assert target_pool["name"] == returned_target_pool["name"]
        assert target_pool["region"] == returned_target_pool["region"]
        assert target_pool.get("resource_id") == returned_target_pool.get("resource_id")
        assert target_pool.get("name") in returned_target_pool.get("name")
        if target_pool.get("backup_pool"):
            assert target_pool.get("backup_pool") in returned_target_pool.get(
                "backup_pool"
            )
        if target_pool.get("failover_ratio"):
            assert target_pool.get("failover_ratio") in returned_target_pool.get(
                "failover_ratio"
            )


@pytest.mark.asyncio
async def test_get_resource_id(hub, ctx, gcp_target_pools):
    if len(gcp_target_pools) > 0:
        target_pool = gcp_target_pools[0]

        ret = await hub.exec.gcp.compute.target_pool.get(
            ctx,
            resource_id=target_pool["resource_id"],
        )

        assert ret["result"], ret["ret"]
        assert not ret["comment"]

        returned_target_pool = ret["ret"]
        assert target_pool["name"] == returned_target_pool["name"]
        assert target_pool["region"] == returned_target_pool["region"]
        assert target_pool.get("resource_id") == returned_target_pool.get("resource_id")
        assert target_pool.get("name") in returned_target_pool.get("name")
        if target_pool.get("backup_pool"):
            assert target_pool.get("backup_pool") in returned_target_pool.get(
                "backup_pool"
            )
        if target_pool.get("failover_ratio"):
            assert target_pool.get("failover_ratio") in returned_target_pool.get(
                "failover_ratio"
            )


@pytest.mark.asyncio
async def test_get_invalid_name(hub, ctx):
    ret = await hub.exec.gcp.compute.target_pool.get(
        ctx, name="invalid-name", region="us-central1"
    )
    assert ret["result"], ret["comment"]
    assert not ret["ret"], ret["comment"]
    assert "Get compute.target_pool 'None' result is empty" in ret["comment"]


@pytest.mark.asyncio
async def test_get_invalid_resource_id(hub, ctx):
    invalid_resource_id = (
        "/projects/invalid/regions/us-central1/targetPools/invalid-name"
    )
    ret = await hub.exec.gcp.compute.target_pool.get(
        ctx,
        resource_id=invalid_resource_id,
    )
    assert not ret["result"], not ret["ret"]
    assert "HttpError" in ret["comment"][0]
