.. idem-gcp documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _README:

.. include:: ../README.rst

.. toctree::
   :maxdepth: 2
   :glob:
   :hidden:

   quickstart/index
   tutorial/index
   releases/index

.. include:: /_includes/reference-toc.rst

.. toctree::
   :caption: Get Involved
   :maxdepth: 2
   :glob:
   :hidden:

   topics/contributing
   topics/license
   Project Repository <https://gitlab.com/vmware/idem/idem-gcp/>

.. toctree::
   :caption: Links
   :hidden:

   Idem Project Docs Portal <https://docs.idemproject.io>
   Idem Project Website <https://www.idemproject.io>
   Idem Project Source on GitLab <https://gitlab.com/vmware/idem>
   POP Project Source on GitLab <https://gitlab.com/vmware/pop>
