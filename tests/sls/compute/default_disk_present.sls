{name}:
  gcp.compute.disk.present:
  - zone: us-central1-a
  - project: tango-gcp
  - type_: projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced
  - size_gb: {size_gb}
  - guest_os_features:
      - type_: VIRTIO_SCSI_MULTIQUEUE
      - type_: SEV_CAPABLE
      - type_: UEFI_COMPATIBLE
      - type_: GVNIC
  - resource_policies: {resource_policies}
  - labels: {labels}
  - label_fingerprint: {label_fingerprint}
