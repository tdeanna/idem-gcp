Install
=======

Idem can be installed using pip either from PyPI or directly from source code.

Python 3.7 or greater required
++++++++++++++++++++++++++++++

Verify you have Python 3.7 or later installed:

.. code-block:: bash

    python -V

.. note::

    You might need to use `pip3` in the commands below.
    Use `pip3` if the output of the following command points to Python 2.x.

    ``pip --version``

    ``pip xx.x.x from /usr/lib/python3/dist-packages/pip (python 3.x)``

Install from PyPI
+++++++++++++++++


Once you've verified you have a supported version of Python run the following
to install Idem.

.. code-block:: bash

    pip install idem


Check your version of Idem.

.. code-block:: bash

    idem --version


Now install `idem-gcp`:


.. code-block:: bash

   pip install idem-gcp


Install idem-gcp from source
++++++++++++++++++++++++++++


Clone the `idem_gcp` repository.

.. code:: bash

    git clone git@gitlab.com:vmware/idem/idem-gcp.git
    cd idem_gcp

Create a virtual environment, and then activate it:

.. code:: bash

    python3 -m venv venv
    source venv/bin/activate

Install idem-gcp and other base requirements:

.. code:: bash

    pip3 install -e .
    pip3 install -r requirements/base.txt


.. include:: ./_includes/install_idem-gcp.rst
