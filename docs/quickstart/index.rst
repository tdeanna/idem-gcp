==========
Quickstart
==========

Get started with **idem-gcp** in 5 minutes.

Idem is an Apache licensed project for managing complex cloud environments.

Idem-gcp is an Idem plugin to allow you to manage many parts of the GCP cloud.


Supported GCP resources
+++++++++++++++++++++++

.. grid:: 2

    .. grid-item-card:: Idem Exec Modules
        :link: /ref/exec/index
        :link-type: doc

        Exec Modules

        :bdg-info:`GCP`

    .. grid-item-card:: Idem State Modules
        :link: /ref/states/index
        :link-type: doc

        State Modules

        :bdg-info:`GCP`



The first step is to :doc:`install Idem and Idem-gcp. </quickstart/install/index>`

.. toctree::
   :maxdepth: 1
   :glob:
   :hidden:

   install/index
   configure/index
   commands/index
   specifics/index
   more/index
