"""Metadata module for managing ForwardingRules."""

PATH = "projects/{project}/regions/{region}/forwardingRules/{forwardingRule}"

NATIVE_RESOURCE_TYPE = "compute.forwarding_rules"
