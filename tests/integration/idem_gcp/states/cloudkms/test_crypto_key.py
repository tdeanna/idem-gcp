import copy

import pytest

from tests.utils import generate_unique_name

PARAMETER = {
    "idem_name": generate_unique_name("idem-test-crypto_key"),
    "project_id": "tango-gcp",
    "location_id": "us-east1",
    "key_ring_id": "cicd-idem-gcp-1",
    "crypto_key_id": "cicd-key-1",
    "crypto_key_version_id": None,
}

RESOURCE_TYPE_CRYPTO_KEY = "cloudkms.crypto_key"
RESOURCE_TYPE_CRYPTO_KEY_PROPERTIES = (
    "cloudkms.projects.locations.key_rings.crypto_keys"
)


PRESENT_WITH_RESOURCE_ID = {
    "resource_id": f'projects/{PARAMETER["project_id"]}/locations/{PARAMETER["location_id"]}/keyRings/{PARAMETER["key_ring_id"]}/cryptoKeys/{PARAMETER["crypto_key_id"]}'
}

PRESENT_STATE = {
    "name": PARAMETER["idem_name"],
    "primary": {
        "name": f"projects/{PARAMETER['project_id']}/locations/{PARAMETER['location_id']}/keyRings/{PARAMETER['key_ring_id']}/cryptoKeys/{PARAMETER['crypto_key_id']}/cryptoKeyVersions/{PARAMETER['crypto_key_version_id']}"
    },
    "purpose": "ENCRYPT_DECRYPT",
    "labels": {"lbl_key_1": "lbl-value-1", "lbl_key_2": "lbl-value-2"},
    "version_template": {
        "algorithm": "GOOGLE_SYMMETRIC_ENCRYPTION",
        "protection_level": "SOFTWARE",
    },
    "destroy_scheduled_duration": "186400s",
    "rotation_period": "31500001s",
    "next_rotation_time": "2034-10-02T15:01:23Z",
    "project_id": f"{PARAMETER['project_id']}",
    "location_id": f"{PARAMETER['location_id']}",
    "key_ring_id": f"{PARAMETER['key_ring_id']}",
    "crypto_key_id": f"{PARAMETER['crypto_key_id']}",
}

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])


@pytest.fixture(scope="function")
async def crypto_key_under_testing(hub, ctx, idem_cli):
    global PARAMETER
    PARAMETER["resource_id"] = PRESENT_WITH_RESOURCE_ID["resource_id"]
    old_get_ret = await hub.exec.gcp.cloudkms.crypto_key.get(
        ctx, resource_id=PARAMETER["resource_id"]
    )
    assert old_get_ret["ret"]
    current_crypto_key_version_id = (
        old_get_ret["ret"].get("primary").get("name").split("/")[-1]
    )
    if current_crypto_key_version_id == "1":
        PRESENT_STATE["primary"][
            "name"
        ] = f"projects/{PARAMETER['project_id']}/locations/{PARAMETER['location_id']}/keyRings/{PARAMETER['key_ring_id']}/cryptoKeys/{PARAMETER['crypto_key_id']}/cryptoKeyVersions/2"
    else:
        PRESENT_STATE["primary"][
            "name"
        ] = f"projects/{PARAMETER['project_id']}/locations/{PARAMETER['location_id']}/keyRings/{PARAMETER['key_ring_id']}/cryptoKeys/{PARAMETER['crypto_key_id']}/cryptoKeyVersions/1"


# This test does not create a new crypto key. It updates
# the primary version of an already existing crypto key
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
def test_crypto_key_present(hub, idem_cli, crypto_key_under_testing, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_CRYPTO_KEY,
        PRESENT_STATE,
        __test,
    )
    assert ret["result"], ret["comment"]
    if __test:
        assert (
            hub.tool.gcp.comment_utils.would_update_comment(
                "gcp.cloudkms.crypto_key", PARAMETER["resource_id"]
            )
            in ret["comment"]
        )

    else:
        assert (
            hub.tool.gcp.comment_utils.update_comment(
                "gcp.cloudkms.crypto_key", PARAMETER["resource_id"]
            )
            in ret["comment"]
        )
    assert ret["old_state"], ret["comment"]
    assert ret["new_state"], ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_get_resource_only_with_resource_id_missing_resource_id",
    depends=["present"],
)
def test_crypto_key_present_get_resource_only_with_resource_id_missing_resource_id(
    hub, idem_cli, tests_dir, __test
):
    global PARAMETER
    present_state = copy.deepcopy(PRESENT_STATE)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_CRYPTO_KEY,
        present_state,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    if __test:
        assert ret["result"], ret["comment"]
        assert (
            hub.tool.gcp.comment_utils.would_create_comment(
                "gcp.cloudkms.crypto_key", PARAMETER["idem_name"]
            )
            in ret["comment"]
        )
    else:
        assert "already exists" in ret["comment"][0]
        assert not ret["result"], ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_get_resource_only_with_resource_id_resource_id", depends=["present"]
)
def test_crypto_key_present_get_resource_only_with_resource_id_resource_id(
    hub, idem_cli, tests_dir, __test
):
    global PARAMETER
    present_state = copy.deepcopy(PRESENT_STATE)
    present_state.update(PRESENT_WITH_RESOURCE_ID)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_CRYPTO_KEY,
        present_state,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    if __test:
        assert ret["result"], ret["comment"]
    else:
        assert ret["result"], ret["comment"]
        assert (
            hub.tool.gcp.comment_utils.up_to_date_comment(
                "gcp.cloudkms.crypto_key", PARAMETER["resource_id"]
            )
            in ret["comment"]
        )
        assert ret["old_state"], ret["comment"]
        assert not ret["new_state"], ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent")
def test_crypto_key_absent(hub, idem_cli, tests_dir, __test):
    global PARAMETER
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_CRYPTO_KEY,
        name=PARAMETER["idem_name"],
        resource_id=PRESENT_WITH_RESOURCE_ID["resource_id"],
        test=__test,
    )
    assert not ret["result"], ret["comment"]
    assert (
        hub.tool.gcp.comment_utils.no_resource_delete_comment("gcp.cloudkms.crypto_key")
        in ret["comment"]
    )


@pytest.mark.asyncio
async def test_crypto_key_describe(hub, ctx):
    ret = await hub.states.gcp.cloudkms.crypto_key.describe(ctx)
    assert len(ret) > 0
    assert ret.get(f"{PRESENT_WITH_RESOURCE_ID['resource_id']}"), ret
