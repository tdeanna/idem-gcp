import copy
import time

import pytest
from pytest_idem import runner

idem_name = ("idem-test-key_ring-" + str(int(time.time())),)
project_id = "tango-gcp"
location_id = "us-east1"
key_ring_id = "cicd-idem-gcp-1"

RESOURCE_TYPE_KEY_RING = "cloudkms.key_ring"

ABSENT_STATE = f"""
{idem_name}:
  gcp.cloudkms.key_ring.absent:
  - resource_id: projects/{project_id}/locations/{location_id}/keyRings/{key_ring_id}
"""

PRESENT_STATE = {
    "name": f"{idem_name}",
    "project_id": f"{project_id}",
    "location_id": f"{location_id}",
    "key_ring_id": f"{key_ring_id}",
}

PRESENT_WITH_RESOURCE_ID = {
    "resource_id": f"projects/{project_id}/locations/{location_id}/keyRings/{key_ring_id}"
}

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
def test_key_ring_present(hub, ctx, idem_cli, tests_dir, __test):
    present_state = copy.deepcopy(PRESENT_STATE)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_KEY_RING,
        present_state,
        __test,
    )

    assert ret["result"], ret["comment"]
    assert ret["old_state"], ret["comment"]
    assert ret["new_state"], ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_get_resource_only_with_resource_id_missing_resource_id",
    depends=["present"],
)
def test_key_ring_present_get_resource_only_with_resource_id_missing_resource_id(
    hub, idem_cli, tests_dir, __test
):
    present_state = copy.deepcopy(PRESENT_STATE)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_KEY_RING,
        present_state,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    if __test:
        assert ret["result"], ret["comment"]
    else:
        assert "already exists" in ret["comment"][0]
        assert not ret["result"], ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_get_resource_only_with_resource_id_resource_id", depends=["present"]
)
def test_key_ring_present_get_resource_only_with_resource_id_resource_id(
    hub, idem_cli, tests_dir, __test
):
    present_state = copy.deepcopy(PRESENT_STATE)
    present_state.update(PRESENT_WITH_RESOURCE_ID)
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_KEY_RING,
        present_state,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    if __test:
        assert ret["result"], ret["comment"]
    else:
        assert ret["result"], ret["comment"]
        assert (
            hub.tool.gcp.comment_utils.already_exists_comment(
                "gcp.cloudkms.key_ring", key_ring_id
            )
            in ret["comment"]
        )
        assert ret["old_state"], ret["comment"]
        assert ret["new_state"], ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent")
def test_key_ring_absent(hub, idem_cli, tests_dir, __test):
    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(ABSENT_STATE)
        if __test:
            state_ret = idem_cli(
                "state",
                fh,
                "--test",
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        else:
            state_ret = idem_cli(
                "state",
                fh,
                "--acct-profile=test_development_idem_gcp",
                check=True,
            ).json
        ret = hub.tool.gcp.utils.get_esm_tagged_data(state_ret, "gcp.cloudkms.key_ring")
        assert not ret["result"], ret["comment"]


@pytest.mark.asyncio
async def test_key_ring_describe(hub, ctx):
    ret = await hub.states.gcp.cloudkms.key_ring.describe(ctx)
    assert len(ret) > 0
