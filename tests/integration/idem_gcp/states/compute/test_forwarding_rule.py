import copy
from collections import ChainMap

import pytest

from idem_gcp.tool.gcp.utils import _is_within
from tests.utils import generate_unique_name

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])

NAME_WITH_TARGET_POOL = generate_unique_name("test-forwarding-rule-t")
NAME_WITH_BACKEND = generate_unique_name("test-forwarding-rule-b")
PARAMETER = {
    "project": "tango-gcp",
    "region": "asia-east1",
    "health_check_name": generate_unique_name("idem-test-forwarding"),
    "backend_service_name": generate_unique_name("idem-test-backend"),
}
RESOURCE_TYPE = "compute.forwarding_rule"
RESOURCE_TYPE_BACKEND_SERVICE = "compute.backend_service"
RESOURCE_TYPE_HEALTH_CHECK = "compute.health_check"
RESOURCE_ID_TARGET_POOL = f"projects/{PARAMETER['project']}/regions/{PARAMETER['region']}/forwardingRules/{NAME_WITH_TARGET_POOL}"
RESOURCE_ID_BACKEND = f"projects/{PARAMETER['project']}/regions/{PARAMETER['region']}/forwardingRules/{NAME_WITH_BACKEND}"

PRESENT_WITH_TARGET_POOL_CREATE_STATE = {
    "name": NAME_WITH_TARGET_POOL,
    "region": PARAMETER["region"],
    # TODO: remove dependencies to target pool or remove the tests
    "target": "projects/tango-gcp/regions/asia-east1/targetPools/ventsy-tcp-pooled-01",
    "ip_protocol": "TCP",
    "port_range": "80-80",
    "load_balancing_scheme": "EXTERNAL",
    "network_tier": "PREMIUM",
}

PRESENT_WITH_BACKEND_CREATE_STATE = {
    "name": NAME_WITH_BACKEND,
    "region": PARAMETER["region"],
    "subnetwork": f"projects/{PARAMETER['project']}/regions/{PARAMETER['region']}/subnetworks/default",
    "network": f"projects/{PARAMETER['project']}/global/networks/default",
    "ip_protocol": "TCP",
    "ports": ["80"],
    "load_balancing_scheme": "INTERNAL",
    "network_tier": "PREMIUM",
}

PRESENT_UPDATED_TARGET = {
    "target": "projects/tango-gcp/regions/asia-east1/targetPools/ventsy-tcp-pooled-02",
}

HEALTH_CHECK_SPEC = {
    "name": PARAMETER["health_check_name"],
    "project": PARAMETER["project"],
    "type_": "TCP",
    "tcp_health_check": {"port": 80},
}

BACKEND_SERVICE_PRESENT = {
    "name": PARAMETER["backend_service_name"],
    "project": PARAMETER["project"],
    "region": PARAMETER["region"],
    "load_balancing_scheme": "INTERNAL",
    "timeout_sec": 60,
    "protocol": "TCP",
    "session_affinity": "NONE",
    "affinity_cookie_ttl_sec": 10,
    "connection_draining": {"draining_timeout_sec": 300},
}

PRESENT_UPDATED_LABELS = {"labels": {"key1": "val1", "key2": "val2"}}

PRESENT_UPDATE_NETWORK_TIER = {"network_tier": "STANDARD"}

PRESENT_UPDATED_IP_ADDRESS = {
    "ip_address": "10.10.10.10",
}

LABEL_FINGERPRINT = ""


@pytest.fixture(scope="module")
async def health_check(hub, ctx, idem_cli):
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_HEALTH_CHECK,
        HEALTH_CHECK_SPEC,
    )

    assert ret["result"], ret["comment"]

    yield ret["new_state"]

    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_HEALTH_CHECK,
        PARAMETER["health_check_name"],
        ret["new_state"]["resource_id"],
    )

    assert ret["result"], ret["comment"]


@pytest.fixture(scope="module")
def backend_service(hub, idem_cli, health_check):
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_BACKEND_SERVICE,
        {
            **BACKEND_SERVICE_PRESENT,
            "health_checks": [health_check["resource_id"]],
        },
    )
    assert ret["result"], ret["comment"]

    yield ret["new_state"]

    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_BACKEND_SERVICE,
        PARAMETER["backend_service_name"],
        ret["new_state"]["resource_id"],
    )

    assert ret["result"], ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_create_target_pool")
def test_forwarding_rule_present_create(hub, idem_cli, tests_dir, __test):
    global LABEL_FINGERPRINT

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PRESENT_WITH_TARGET_POOL_CREATE_STATE, __test
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE}", name=NAME_WITH_TARGET_POOL
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE}", name=NAME_WITH_TARGET_POOL
            )
            in ret["comment"]
        )

    expected_state = {
        "resource_id": RESOURCE_ID_TARGET_POOL,
        **PRESENT_WITH_TARGET_POOL_CREATE_STATE,
    }

    assert ret["new_state"]
    # GCP changes the IP so we have to update here
    PRESENT_WITH_TARGET_POOL_CREATE_STATE["ip_address"] = ret["new_state"].get(
        "ip_address"
    )

    if not __test:
        assert "label_fingerprint" in ret["new_state"]
        LABEL_FINGERPRINT = ret["new_state"].get("label_fingerprint")

    # we can't know the ip_address upfront - ignore it in the comparison
    assert _is_within(
        ret["new_state"],
        expected_state,
        {"id", "kind", "creation_timestamp", "label_fingerprint", "ip_address"},
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="set_network_tier",
    depends=["present_create_target_pool"],
)
async def test_set_network_tier(hub, idem_cli, __test):
    # TODO: A test without --test flag currently cannot be written because the setup for standard network tier requires a complex infrastructure of resources.
    if __test:
        update_present_state = copy.deepcopy(PRESENT_WITH_TARGET_POOL_CREATE_STATE)
        update_present_state.update(PRESENT_UPDATE_NETWORK_TIER)

        ret = hub.tool.utils.call_present_from_properties(
            idem_cli, RESOURCE_TYPE, update_present_state, __test
        )

        assert ret["result"], ret["comment"]
        assert ret["new_state"]
        # TODO: the operation completes but the tier doesn't update
        # assert ret["new_state"].get("network_tier") == PRESENT_UPDATE_NETWORK_TIER.get("network_tier")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="set_labels",
    depends=["set_network_tier"],
)
async def test_set_labels(hub, idem_cli, __test):
    update_present_state = copy.deepcopy(PRESENT_WITH_TARGET_POOL_CREATE_STATE)
    update_present_state.update(PRESENT_UPDATED_LABELS)
    update_present_state.update({"label_fingerprint": LABEL_FINGERPRINT})

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, update_present_state, __test
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"]
    assert ret["new_state"].get("labels") == PRESENT_UPDATED_LABELS.get("labels")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="set_target",
    depends=["set_labels"],
)
async def test_set_target(hub, idem_cli, __test):
    update_present_state = copy.deepcopy(PRESENT_WITH_TARGET_POOL_CREATE_STATE)
    update_present_state.update(PRESENT_UPDATED_TARGET)
    update_present_state.update(PRESENT_UPDATED_LABELS)

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, update_present_state, __test
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"]
    assert ret["new_state"].get("target") == PRESENT_UPDATED_TARGET.get("target")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="set_ip_address",
    depends=["present_create_target_pool"],
)
async def test_set_ip_address_should_fail(hub, idem_cli, __test):
    update_present_state = copy.deepcopy(PRESENT_WITH_TARGET_POOL_CREATE_STATE)
    update_present_state.update(PRESENT_UPDATED_IP_ADDRESS)

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, update_present_state, __test
    )

    assert not ret["result"]
    assert ret.get("comment")
    assert any(
        "Forbidden modification of non-updatable properties" in c
        for c in ret["comment"]
    )


@pytest.mark.asyncio
async def test_describe(hub, ctx):
    ret = await hub.states.gcp.compute.forwarding_rule.describe(ctx)
    for resource_id in ret:
        assert "gcp.compute.forwarding_rule.present" in ret[resource_id]
        described_resource = ret[resource_id].get("gcp.compute.forwarding_rule.present")
        assert described_resource
        forwarding_rule = dict(ChainMap(*described_resource))
        assert forwarding_rule.get("resource_id") == resource_id


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
async def test_absent_invalid_resource_id(hub, idem_cli, __test):
    invalid_resource_id = RESOURCE_ID_TARGET_POOL[:-5] + "dummy"
    ret = hub.tool.utils.call_absent(
        idem_cli, RESOURCE_TYPE, None, invalid_resource_id, test=__test
    )

    # Considers the resource deleted.
    assert ret
    assert ret.get("comment")
    assert any("already absent" in c for c in ret["comment"])


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="test_absent",
    depends=["present_create_target_pool"],
)
async def test_absent(hub, idem_cli, __test):
    ret = hub.tool.utils.call_absent(
        idem_cli, RESOURCE_TYPE, None, RESOURCE_ID_TARGET_POOL, test=__test
    )

    if __test:
        assert ret
        assert ret.get("comment")
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE}", name=NAME_WITH_TARGET_POOL
            )
        ] == ret["comment"]
    else:
        # Considers the resource deleted.
        assert ret
        assert ret.get("comment")
        assert [
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE}", name=NAME_WITH_TARGET_POOL
            )
        ] == ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_create_backend",
)
def test_forwarding_rule_present_backend_create(
    hub, idem_cli, tests_dir, __test, backend_service
):
    PRESENT_WITH_BACKEND_CREATE_STATE["backend_service"] = backend_service[
        "resource_id"
    ]
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE, PRESENT_WITH_BACKEND_CREATE_STATE, __test
    )

    assert ret["result"], ret["comment"]
    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE}", name=NAME_WITH_BACKEND
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE}", name=NAME_WITH_BACKEND
            )
            in ret["comment"]
        )

    expected_state = {
        "resource_id": RESOURCE_ID_BACKEND,
        **PRESENT_WITH_BACKEND_CREATE_STATE,
    }

    assert ret["new_state"]

    # we can't know the ip_address upfront - ignore it in the comparison
    assert _is_within(
        ret["new_state"],
        expected_state,
        {"id", "kind", "creation_timestamp", "label_fingerprint", "ip_address"},
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="test_absent_backend",
    depends=["present_create_backend"],
)
async def test_absent_backend(hub, idem_cli, __test):
    ret = hub.tool.utils.call_absent(
        idem_cli, RESOURCE_TYPE, None, RESOURCE_ID_BACKEND, test=__test
    )

    if __test:
        assert ret
        assert ret.get("comment")
        assert [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE}", name=NAME_WITH_BACKEND
            )
        ] == ret["comment"]
    else:
        # Considers the resource deleted.
        assert ret
        assert ret.get("comment")
        assert [
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE}", name=NAME_WITH_BACKEND
            )
        ] == ret["comment"]
