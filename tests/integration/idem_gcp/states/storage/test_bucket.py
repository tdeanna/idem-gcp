import copy
from collections import ChainMap
from typing import Any
from typing import Dict

import pytest

from tests.utils import compare_expected_state
from tests.utils import generate_unique_name

RESOURCE_TYPE_BUCKET = "storage.bucket"
RESOURCE_TYPE_ENCRYPTION_KEY = "cloudkms.crypto_key"
RESOURCE_TYPE_ENCRYPTION_KEY_RING = "cloudkms.key_ring"
RESOURCE_TYPE_ENCRYPTION_KEY_VERSION = "cloudkms.crypto_key_version"

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETRIZE_WITH_RESOURCE_ID_FLAG = dict(
    argnames="__test,__resource_id_flag",
    argvalues=[(True, True), (True, False), (False, True), (False, False)],
    ids=[
        "--test-resource-id-flag",
        "--test-no-resource-id-flag",
        "--run-resource-id-flag",
        "--run-no-resource-id-flag",
    ],
)

PARAMETER = {"name": generate_unique_name("idem-test-bucket-defaults")}

PRESENT_ALL_VALUES = {
    "autoclass": {"enabled": False},
    "billing": {"requester_pays": True},
    "cors": [
        {
            "max_age_seconds": 5,
            "origin": ["*"],
            "method": ["GET", "POST"],
            "response_header": [],
        },
        {
            "max_age_seconds": 7,
            "origin": ["https://google.com", "http://example.com"],
            "method": ["PUT"],
            "response_header": ["Keep-Alive", "Set-Cookie", "Content-Type"],
        },
    ],
    "custom_placement_config": {"data_locations": ["EUROPE-NORTH1", "EUROPE-WEST1"]},
    "default_event_based_hold": True,
    "encryption": {"default_kms_key_name": "some_kms_key_name"},
    "iam_configuration": {
        "public_access_prevention": "enforced",
        "uniform_bucket_level_access": {"enabled": False},
    },
    "labels": {"key": "value", "snake_cased": "value2"},
    "lifecycle": {
        "rule": [
            {
                "condition": {
                    "age": 5,
                    "created_before": "1999-04-19",
                    "custom_time_before": "1999-04-19",
                    "days_since_custom_time": 5,
                    "days_since_noncurrent_time": 5,
                    "is_live": True,
                    "matches_prefix": ["prefix1", "prefix2"],
                    "matches_storage_class": ["NEARLINE", "COLDLINE"],
                    "matches_suffix": ["suffix1", "suffix2"],
                    "noncurrent_time_before": "1999-04-19",
                    "num_newer_versions": 3,
                },
                "action": {"storage_class": "STANDARD", "type_": "SetStorageClass"},
            },
            {"condition": {"age": 4}, "action": {"type_": "Delete"}},
        ]
    },
    "location": "EU",
    "logging": {
        "log_bucket": "log-bucket-second-try",
        "log_object_prefix": "customprefix",
    },
    "retention_policy": {
        "retention_period": "10000",
    },
    "rpo": "ASYNC_TURBO",
    "storage_class": "NEARLINE",
    "versioning": {"enabled": False},
    "website": {"main_page_suffix": "index", "not_found_page": "notfound"},
    "acl": [
        {
            "role": "OWNER",
            "entity": "project-owners-103179964387",
        },
        {
            "role": "OWNER",
            "entity": "project-editors-103179964387",
        },
    ],
    "default_object_acl": [
        {
            "role": "OWNER",
            "entity": "project-owners-103179964387",
        },
        {
            "role": "READER",
            "entity": "project-viewers-103179964387",
        },
    ],
}

PRESENT_EXPECTED_DEFAULT_VALUES = {
    "autoclass": {"enabled": False},
    "billing": {"requester_pays": False},
    "cors": [],
    "custom_placement_config": None,
    "encryption": None,
    "default_event_based_hold": False,
    "iam_configuration": {
        "public_access_prevention": "inherited",
        "uniform_bucket_level_access": {"enabled": False},
    },
    "labels": {},
    "lifecycle": {"rule": []},
    "location": "US",
    "logging": None,
    "retention_policy": None,
    "storage_class": "STANDARD",
    "versioning": {"enabled": False},
    "website": {"main_page_suffix": None, "not_found_page": None},
}

PRESENT_UPDATE_PROPERTIES = {
    "labels": {"label_1": "value_1"},
    "storage_class": "COLDLINE",
    "versioning": {"enabled": True},
    "acl": [
        {
            "role": "OWNER",
            "entity": "project-owners-103179964387",
        },
        {
            "role": "WRITER",
            "entity": "project-editors-103179964387",
        },
        {
            "role": "READER",
            "entity": "project-viewers-103179964387",
        },
    ],
    "default_object_acl": [
        {
            "role": "OWNER",
            "entity": "project-owners-103179964387",
        },
    ],
}

PRESENT_UPDATED_STATE: Dict[str, Any] = {
    "name": PARAMETER["name"],
    **PRESENT_EXPECTED_DEFAULT_VALUES,
    **PRESENT_UPDATE_PROPERTIES,
}

RETENTION_POLICY_PROPERTIES = {
    "retention_policy": {"retention_period": "20", "is_locked": True},
    # versioning and retention policy are mutually exclusive features
    "versioning": {"enabled": False},
}

# static bucket encryption key to be used in tests
encryption_key_project_id = "tango-gcp"
encryption_key_location_id = "europe"
encryption_key_ring_id = "cicd-idem-gcp-bucket-encryption-key-ring-id"
encryption_key_id = "cicd-idem-gcp-bucket-encryption-key-id"

# Data format { "the_resource_id": { "name": "name", "resource_type": "type", "absent_params": {...} } }
RESOURCES_TO_DELETE = {}


@pytest.fixture(scope="function")
def encryption_key_ring(hub, idem_cli):
    encryption_key_ring_name = "cicd-idem-gcp-bucket-encryption-key-ring"
    present_state = {
        "name": encryption_key_ring_name,
        "project_id": encryption_key_project_id,
        "location_id": encryption_key_location_id,
        "key_ring_id": encryption_key_ring_id,
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_ENCRYPTION_KEY_RING, present_state
    )
    assert ret["result"], ret["comment"]
    key_ring = ret["new_state"]
    assert key_ring

    # no absent function for key rings
    return key_ring


@pytest.fixture(scope="function")
def encryption_key(hub, idem_cli, encryption_key_ring):
    encryption_key_name = "cicd-idem-gcp-bucket-encryption-key"
    present_state = {
        "name": encryption_key_name,
        "project_id": encryption_key_project_id,
        "location_id": encryption_key_location_id,
        "key_ring_id": encryption_key_ring_id,
        "crypto_key_id": encryption_key_id,
        "purpose": "ENCRYPT_DECRYPT",
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_ENCRYPTION_KEY, present_state
    )
    assert ret["result"], ret["comment"]

    key = ret["new_state"]
    assert key

    # no absent function for keys
    return key


@pytest.fixture(scope="function")
def encryption_key_version(hub, idem_cli, encryption_key, encryption_key_ring):
    encryption_key_version_name = "cicd-idem-gcp-bucket-encryption-key-version"
    present_state = {
        "name": encryption_key_version_name,
        "key_state": "ENABLED",
        "project_id": encryption_key_project_id,
        "location_id": encryption_key_location_id,
        "key_ring_id": encryption_key_ring_id,
        "crypto_key_id": encryption_key_id,
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_ENCRYPTION_KEY_VERSION, present_state
    )
    assert ret["result"], ret["comment"]
    key_version = ret["new_state"]
    assert key_version

    # no absent function for key versions
    return key_version


@pytest.fixture(scope="function")
def encryption_key_with_version(
    hub, idem_cli, encryption_key_ring, encryption_key_version
):
    encryption_key_name = "cicd-idem-gcp-bucket-encryption-key"
    present_state = {
        "name": encryption_key_name,
        "project_id": encryption_key_project_id,
        "location_id": encryption_key_location_id,
        "key_ring_id": encryption_key_ring_id,
        "crypto_key_id": encryption_key_id,
        "purpose": "ENCRYPT_DECRYPT",
        "primary": {"name": encryption_key_version.get("resource_id")},
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_ENCRYPTION_KEY, present_state
    )
    assert ret["result"], ret["comment"]

    key = ret["new_state"]
    assert key

    # no absent function for keys
    return key


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_create")
def test_bucket_present_create_default_values(hub, idem_cli, tests_dir, __test):
    bucket_name = PARAMETER["name"]
    required_props = {"name": bucket_name}
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_BUCKET, required_props, __test
    )

    assert ret["result"], ret["comment"]

    resource_id = f"b/{bucket_name}"

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
            )
        ] == ret["comment"]
    else:
        assert (
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
            )
            in ret["comment"]
        )
        assert ret.get("new_state", {}).get("resource_id")
        RESOURCES_TO_DELETE[ret["new_state"]["resource_id"]] = {
            "name": bucket_name,
            "resource_type": RESOURCE_TYPE_BUCKET,
        }

    assert ret["old_state"] is None
    expected_bucket_state = {
        "resource_id": resource_id,
        "name": bucket_name,
        **PRESENT_EXPECTED_DEFAULT_VALUES,
    }
    created_bucket = ret["new_state"]
    compare_expected_state(
        hub, created_bucket, expected_bucket_state, RESOURCE_TYPE_BUCKET
    )
    PARAMETER["resource_id"] = resource_id


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present_create"])
async def test_bucket_describe(hub, ctx):
    describe_ret = await hub.states.gcp.storage.bucket.describe(ctx)
    existing_resource_id = PARAMETER["resource_id"]
    assert existing_resource_id in describe_ret
    assert f"gcp.{RESOURCE_TYPE_BUCKET}.present" in describe_ret[existing_resource_id]
    described_resource = describe_ret[existing_resource_id].get(
        f"gcp.{RESOURCE_TYPE_BUCKET}.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    expected_bucket_state = {
        "resource_id": existing_resource_id,
        "name": PARAMETER["name"],
        **PRESENT_EXPECTED_DEFAULT_VALUES,
    }
    compare_expected_state(
        hub, described_resource_map, expected_bucket_state, RESOURCE_TYPE_BUCKET
    )


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present_update", depends=["describe"])
def test_bucket_present_update(hub, idem_cli, tests_dir, __test):
    bucket_name = PARAMETER["name"]
    updated_props = {
        "resource_id": f"b/{bucket_name}",
        "name": bucket_name,
        **PRESENT_UPDATE_PROPERTIES,
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_BUCKET, updated_props, __test
    )
    assert ret["result"], ret["comment"]
    assert ret["comment"]

    resource_id = f"b/{bucket_name}"

    expected_old_state = {
        "resource_id": resource_id,
        "name": bucket_name,
        **PRESENT_EXPECTED_DEFAULT_VALUES,
    }
    old_state = ret["old_state"]
    compare_expected_state(hub, old_state, expected_old_state, RESOURCE_TYPE_BUCKET)

    PRESENT_UPDATED_STATE["resource_id"] = resource_id
    expected_new_state = PRESENT_UPDATED_STATE
    new_state = ret["new_state"]
    compare_expected_state(hub, new_state, expected_new_state, RESOURCE_TYPE_BUCKET)

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
            )
        ] == ret["comment"]
    else:
        assert [
            hub.tool.gcp.comment_utils.update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
            )
        ] == ret["comment"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE_WITH_RESOURCE_ID_FLAG)
@pytest.mark.dependency(name="present_update_noop", depends=["present_update"])
async def test_bucket_present_update_noop(
    hub, ctx, idem_cli, tests_dir, __test, __resource_id_flag
):
    bucket_name = PARAMETER["name"]
    get_ret = await hub.exec.gcp.storage.bucket.get(ctx, name=bucket_name)
    assert get_ret.get("result")
    assert get_ret.get("ret")
    bucket = get_ret["ret"]
    assert bucket.get("resource_id")

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_BUCKET,
        bucket,
        __test,
        additional_kwargs=["--get-resource-only-with-resource-id"]
        if __resource_id_flag
        else [],
    )

    assert ret["result"], ret["comment"]

    old_state = ret["old_state"]
    compare_expected_state(hub, old_state, bucket, RESOURCE_TYPE_BUCKET)

    new_state = ret["new_state"]
    compare_expected_state(hub, new_state, bucket, RESOURCE_TYPE_BUCKET)

    assert [
        hub.tool.gcp.comment_utils.up_to_date_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
        )
    ] == ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_lock_retention_policy_failure", depends=["present_update_noop"]
)
def test_bucket_present_update_lock_retention_policy_wrong_metageneration_fails(
    hub, idem_cli, tests_dir, __test
):
    bucket_name = PARAMETER["name"]
    updated_props = {
        "resource_id": f"b/{bucket_name}",
        "name": bucket_name,
        "if_metageneration_match": "892457",
        **RETENTION_POLICY_PROPERTIES,
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_BUCKET, updated_props, __test
    )

    old_state = ret["old_state"]
    compare_expected_state(hub, old_state, PRESENT_UPDATED_STATE, RESOURCE_TYPE_BUCKET)
    expected_state = copy.deepcopy(
        {**PRESENT_UPDATED_STATE, **RETENTION_POLICY_PROPERTIES}
    )

    if __test:
        assert ret["result"]
        assert [
            hub.tool.gcp.comment_utils.would_update_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
            ),
            f"Would lock retention policy for gcp.{RESOURCE_TYPE_BUCKET} '{bucket_name}'.",
        ] == ret["comment"]
    else:
        assert not ret["result"]
        assert len(ret["comment"]) >= 2
        assert (
            f"Failed to lock retention policy for gcp.{RESOURCE_TYPE_BUCKET} '{bucket_name}'."
            in ret["comment"]
        )

        # locking should fail, but regular update of retention policy is successful
        expected_state["retention_policy"].pop("is_locked")
        PRESENT_UPDATED_STATE.update(expected_state)

    new_state = ret["new_state"]
    compare_expected_state(hub, new_state, expected_state, RESOURCE_TYPE_BUCKET)


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="present_lock_retention_policy",
    depends=["present_lock_retention_policy_failure"],
)
def test_bucket_present_update_lock_retention_policy(hub, idem_cli, tests_dir, __test):
    bucket_name = PARAMETER["name"]
    updated_props = {
        "resource_id": f"b/{bucket_name}",
        "name": bucket_name,
        **RETENTION_POLICY_PROPERTIES,
    }
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_BUCKET, updated_props, __test
    )
    assert ret["result"], ret["comment"]
    assert ret["comment"]

    old_state = ret["old_state"]
    compare_expected_state(hub, old_state, PRESENT_UPDATED_STATE, RESOURCE_TYPE_BUCKET)

    expected_state = {**PRESENT_UPDATED_STATE, **RETENTION_POLICY_PROPERTIES}
    new_state = ret["new_state"]
    compare_expected_state(hub, new_state, expected_state, RESOURCE_TYPE_BUCKET)

    if __test:
        assert [
            f"Would lock retention policy for gcp.{RESOURCE_TYPE_BUCKET} '{bucket_name}'."
        ] == ret["comment"]
    else:
        assert [
            f"Retention policy locked for gcp.{RESOURCE_TYPE_BUCKET} '{bucket_name}'."
        ] == ret["comment"]
        PRESENT_UPDATED_STATE.update(expected_state)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="last_update_test", depends=["present_update"])
async def test_bucket_present_update_resource_id_flag_no_resource_id(
    hub, ctx, idem_cli, tests_dir, __test
):
    bucket_name = PARAMETER["name"]
    get_ret = await hub.exec.gcp.storage.bucket.get(ctx, name=bucket_name)
    assert get_ret.get("result")
    assert get_ret.get("ret")
    bucket = get_ret["ret"]
    bucket.pop("resource_id")

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_BUCKET,
        bucket,
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    if __test:
        assert ret["result"], ret["comment"]
        assert ret["comment"] == [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
            ),
            f"Would lock retention policy for gcp.{RESOURCE_TYPE_BUCKET} '{bucket_name}'.",
        ]
        assert not ret["old_state"]
        compare_expected_state(
            hub, ret["new_state"], {**bucket, "resource_id": None}, RESOURCE_TYPE_BUCKET
        )
    else:
        assert not ret["result"]
        assert ret["comment"]
        assert ret["comment"] == [
            hub.tool.gcp.comment_utils.already_exists_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
            ),
        ]
        assert not ret["old_state"]
        assert not ret["new_state"]


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(
    name="absent",
    depends=["last_update_test"],
)
async def test_bucket_absent(hub, ctx, idem_cli, tests_dir, __test):
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_BUCKET,
        PARAMETER["name"],
        PARAMETER["resource_id"],
        test=__test,
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"] is None

    old_state = ret["old_state"]
    compare_expected_state(hub, old_state, PRESENT_UPDATED_STATE, RESOURCE_TYPE_BUCKET)

    if __test:
        assert ret["comment"] == [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=PARAMETER["name"]
            )
        ]
    else:
        assert ret["comment"] == [
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=PARAMETER["name"]
            )
        ]
        # check that the resource is truly deleted
        get_ret = await hub.exec.gcp.storage.bucket.get(
            ctx, resource_id=PARAMETER["resource_id"]
        )
        assert get_ret["result"], get_ret["comment"]
        assert not get_ret["ret"]

        RESOURCES_TO_DELETE.pop(PARAMETER["resource_id"], None)


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(depends=["absent"])
def test_bucket_absent_already_absent(hub, idem_cli, tests_dir, __test):
    ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_BUCKET,
        PARAMETER["name"],
        PARAMETER["resource_id"],
        test=__test,
    )

    assert ret["result"], ret["comment"]
    assert ret["old_state"] is None
    assert ret["new_state"] is None

    assert ret["comment"] == [
        hub.tool.gcp.comment_utils.already_absent_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=PARAMETER["name"]
        )
    ]


# For the encryption key setting to work, the Cloud Storage service account needs to have the
# Cloud KMS CryptoKey Encrypter/Decrypter role for the encryption key - configure IAM access.
# See: https://cloud.google.com/storage/docs/encryption/using-customer-managed-keys#service-agent-access
async def test_bucket_present_create_all_values(hub, idem_cli, ctx, tests_dir):
    bucket_name = generate_unique_name("idem-test-bucket-all-values")
    PRESENT_ALL_VALUES["encryption"][
        "default_kms_key_name"
    ] = "projects/tango-gcp/locations/europe/keyRings/cicd-idem-gcp-bucket-encryption-key-ring-id/cryptoKeys/cicd-idem-gcp-bucket-encryption-key-id"

    all_bucket_props = {"name": bucket_name, **PRESENT_ALL_VALUES}
    all_present_props = {**all_bucket_props, "user_project": "tango-gcp"}
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_BUCKET, all_present_props
    )

    assert ret["result"], ret["comment"]

    resource_id = f"b/{bucket_name}"

    assert (
        hub.tool.gcp.comment_utils.create_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
        )
        in ret["comment"]
    )
    assert ret.get("new_state", {}).get("resource_id")
    RESOURCES_TO_DELETE[resource_id] = {
        "name": bucket_name,
        "resource_type": RESOURCE_TYPE_BUCKET,
        "absent_params": {"user_project": "tango-gcp"},
    }

    expected_bucket_state = {"resource_id": resource_id, **all_bucket_props}
    created_bucket = ret["new_state"]

    compare_expected_state(
        hub, created_bucket, expected_bucket_state, RESOURCE_TYPE_BUCKET
    )

    # use --get-resource-only-with-resource-id to check that deletion is
    # successful with it and a provided resource_id
    absent_ret = hub.tool.utils.call_absent(
        idem_cli,
        RESOURCE_TYPE_BUCKET,
        bucket_name,
        resource_id,
        absent_params={"user_project": "tango-gcp"},
        additional_kwargs=["--get-resource-only-with-resource-id"],
    )
    assert absent_ret["result"]
    RESOURCES_TO_DELETE.pop(resource_id, None)


async def test_bucket_present_autoclass_enabled(hub, idem_cli, ctx, tests_dir):
    bucket_name = generate_unique_name("idem-test-bucket-autoclass")
    all_props = {"name": bucket_name, "autoclass": {"enabled": True}}
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_BUCKET, all_props
    )

    assert ret["result"], ret["comment"]

    assert (
        hub.tool.gcp.comment_utils.create_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
        )
        in ret["comment"]
    )

    resource_id = f"b/{bucket_name}"
    RESOURCES_TO_DELETE[resource_id] = {
        "name": bucket_name,
        "resource_type": RESOURCE_TYPE_BUCKET,
    }

    expected_bucket_state = {"resource_id": resource_id, **all_props}
    created_bucket = ret["new_state"]

    compare_expected_state(
        hub, created_bucket, expected_bucket_state, RESOURCE_TYPE_BUCKET
    )

    absent_ret = hub.tool.utils.call_absent(
        idem_cli, RESOURCE_TYPE_BUCKET, bucket_name, resource_id
    )
    assert absent_ret["result"]
    RESOURCES_TO_DELETE.pop(resource_id, None)


@pytest.mark.parametrize(**PARAMETRIZE)
def test_bucket_present_create_with_locked_retention_policy(
    hub, idem_cli, ctx, tests_dir, __test
):
    bucket_name = generate_unique_name("idem-test-bucket-locked-retention-policy")
    all_props = {"name": bucket_name, **RETENTION_POLICY_PROPERTIES}
    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_BUCKET, all_props, test=__test
    )

    assert ret["result"], ret["comment"]

    resource_id = f"b/{bucket_name}"

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
            ),
            f"Would lock retention policy for gcp.{RESOURCE_TYPE_BUCKET} '{bucket_name}'.",
        ] == ret["comment"]
    else:
        assert [
            hub.tool.gcp.comment_utils.create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
            ),
            f"Retention policy locked for gcp.{RESOURCE_TYPE_BUCKET} '{bucket_name}'.",
        ] == ret["comment"]

        RESOURCES_TO_DELETE[resource_id] = {
            "name": bucket_name,
            "resource_type": RESOURCE_TYPE_BUCKET,
        }

    expected_bucket_state = {"resource_id": resource_id, **all_props}
    created_bucket = ret["new_state"]

    compare_expected_state(
        hub, created_bucket, expected_bucket_state, RESOURCE_TYPE_BUCKET
    )

    absent_ret = hub.tool.utils.call_absent(
        idem_cli, RESOURCE_TYPE_BUCKET, bucket_name, resource_id
    )
    assert absent_ret["result"]
    RESOURCES_TO_DELETE.pop(resource_id, None)


@pytest.fixture(scope="module")
async def externally_created_bucket(hub, ctx):
    bucket_name = generate_unique_name("idem-test-bucket-externally-created")
    required_props = {"name": bucket_name}

    # create the instance not going through state
    create_ret = await hub.exec.gcp_api.client.storage.bucket.insert(
        ctx,
        name=bucket_name,
        project="tango-gcp",
        body=required_props,
    )

    assert create_ret["result"]

    bucket = create_ret["ret"]
    yield bucket

    del_ret = await hub.exec.gcp_api.client.storage.bucket.delete(
        ctx, resource_id=bucket["resource_id"]
    )

    assert del_ret["result"] or "HttpError 404" in del_ret["comment"][0]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.asyncio
async def test_bucket_present_get_resource_only_with_resource_id_missing_resource_id_already_exists(
    hub, ctx, idem_cli, tests_dir, __test, externally_created_bucket
):
    bucket_name = externally_created_bucket["name"]

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli,
        RESOURCE_TYPE_BUCKET,
        {"name": bucket_name},
        __test,
        ["--get-resource-only-with-resource-id"],
    )

    if __test:
        assert [
            hub.tool.gcp.comment_utils.would_create_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
            )
        ] == ret["comment"]
        assert not ret["old_state"]
        assert ret["new_state"] == {
            **PRESENT_EXPECTED_DEFAULT_VALUES,
            "name": bucket_name,
        }
    else:
        assert not ret["result"]
        assert ret["comment"]
        assert [
            hub.tool.gcp.comment_utils.already_exists_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
            ),
        ] == ret["comment"]
        assert not ret["old_state"]
        assert not ret["new_state"]


@pytest.mark.parametrize(**PARAMETRIZE)
def test_bucket_present_discover_external_bucket(
    hub, idem_cli, tests_dir, __test, externally_created_bucket
):
    bucket_name = externally_created_bucket["name"]
    resource_id = externally_created_bucket["resource_id"]

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_BUCKET, {"name": bucket_name}, __test
    )

    assert ret["result"], ret["comment"]
    compare_expected_state(
        hub, ret["old_state"], PRESENT_EXPECTED_DEFAULT_VALUES, RESOURCE_TYPE_BUCKET
    )
    compare_expected_state(
        hub, ret["new_state"], PRESENT_EXPECTED_DEFAULT_VALUES, RESOURCE_TYPE_BUCKET
    )

    assert [
        hub.tool.gcp.comment_utils.resource_discovered_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", resource_id=resource_id
        ),
        hub.tool.gcp.comment_utils.up_to_date_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
        ),
    ] == ret["comment"]


@pytest.mark.parametrize(**PARAMETRIZE)
def test_bucket_present_resource_id_provided_non_existent_resource_fails(
    hub, idem_cli, tests_dir, __test
):
    bucket_name = "non-existent-bucket"
    resource_id = f"b/{bucket_name}"

    present_props = {"name": bucket_name, "resource_id": resource_id}

    ret = hub.tool.utils.call_present_from_properties(
        idem_cli, RESOURCE_TYPE_BUCKET, present_props, __test
    )

    assert not ret["result"]
    assert not ret["old_state"]
    assert not ret["new_state"]
    assert (
        hub.tool.gcp.comment_utils.resource_not_found_comment(
            "gcp.storage.bucket", resource_id
        )
        in ret["comment"]
    )


# we want this test to be executed while externally_created_bucket still exists
# to make sure that it doesn't get discovered
# it should not delete the bucket
# so execute it before test_bucket_absent_delete_external_instance_by_name
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="external_resource_not_deleted")
def test_bucket_absent_get_resource_only_with_resource_id_missing_resource_id_already_absent(
    hub, idem_cli, tests_dir, __test, externally_created_bucket
):
    bucket_name = externally_created_bucket["name"]
    absent_ret = hub.tool.utils.call_absent(
        idem_cli,
        resource_type=RESOURCE_TYPE_BUCKET,
        name=bucket_name,
        resource_id=None,
        test=__test,
        additional_kwargs=["--get-resource-only-with-resource-id"],
    )
    assert absent_ret["result"]
    assert not absent_ret["old_state"]
    assert not absent_ret["new_state"]
    assert absent_ret["comment"] == [
        hub.tool.gcp.comment_utils.already_absent_comment(
            resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
        )
    ]


@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(depends=["external_resource_not_deleted"])
def test_bucket_absent_delete_external_instance_by_name(
    hub, idem_cli, tests_dir, __test, externally_created_bucket
):
    bucket_name = externally_created_bucket["name"]
    absent_ret = hub.tool.utils.call_absent(
        idem_cli,
        resource_type=RESOURCE_TYPE_BUCKET,
        name=bucket_name,
        resource_id=None,
        test=__test,
    )
    assert absent_ret["result"]
    assert not absent_ret["new_state"]
    compare_expected_state(
        hub,
        absent_ret["old_state"],
        PRESENT_EXPECTED_DEFAULT_VALUES,
        RESOURCE_TYPE_BUCKET,
    )
    if __test:
        assert absent_ret["comment"] == [
            hub.tool.gcp.comment_utils.would_delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
            )
        ]
    else:
        assert (
            hub.tool.gcp.comment_utils.delete_comment(
                resource_type=f"gcp.{RESOURCE_TYPE_BUCKET}", name=bucket_name
            )
            in absent_ret["comment"]
        )


@pytest.fixture(autouse=True, scope="module")
async def test_cleanup(hub, ctx, idem_cli):
    yield

    # Do cleanup
    for resource_id, resource_data in RESOURCES_TO_DELETE.items():
        ret = hub.tool.utils.call_absent(
            idem_cli,
            resource_data["resource_type"],
            resource_data["name"],
            resource_id,
            absent_params=resource_data.get("absent_params"),
        )
        assert ret["result"], ret["comment"]
        assert not ret.get("new_state")
