def test_sanitize_resource_urls(hub):
    assert not hub.tool.gcp.sanitizers.sanitize_resource_urls(None)

    resource = {
        "zone": "us-central1-a",
        "machine_type": "https://compute.googleapis.com/compute/v1/projects/sample-project/zones/us-central1-a/machineTypes/e2-medium",
        "network_interfaces": [
            {
                "network": "https://www.googleapis.com/compute/v1/projects/sample-project/global/networks/default"
            }
        ],
        "self_link": "https://storage.googleapis.com/storage/v1/b/test-bucket-1/acl/project-viewers-103179964387",
        "service_accounts": {
            "email": "123456789012-compute@developer.gserviceaccount.com",
            "scopes": [
                "https://www.googleapis.com/auth/devstorage.read_only",
                "https://www.googleapis.com/auth/servicecontrol",
                "https://www.googleapis.com/auth/service.management.readonly",
            ],
        },
    }

    sanitized_resource = hub.tool.gcp.sanitizers.sanitize_resource_urls(resource)

    assert (
        sanitized_resource["machine_type"]
        == "projects/sample-project/zones/us-central1-a/machineTypes/e2-medium"
    )
    assert (
        sanitized_resource["network_interfaces"][0]["network"]
        == "projects/sample-project/global/networks/default"
    )
    assert (
        sanitized_resource["service_accounts"]["scopes"]
        == resource["service_accounts"]["scopes"]
    )
    assert (
        sanitized_resource["self_link"]
        == "b/test-bucket-1/acl/project-viewers-103179964387"
    )
