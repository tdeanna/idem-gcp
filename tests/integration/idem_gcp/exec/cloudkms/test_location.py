import pytest


@pytest.mark.asyncio
async def test_location_list(hub, ctx):
    ret = await hub.exec.gcp.cloudkms.location.list(ctx, project=ctx.acct.project_id)
    assert ret["result"], ret["comment"]
    assert len(ret["ret"]) > 0


@pytest.mark.asyncio
async def test_location_get(hub, ctx):
    ret = await hub.exec.gcp.cloudkms.location.get(
        ctx, resource_id=f"projects/{ctx.acct.project_id}/locations/global"
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]["location_id"] == "global"
