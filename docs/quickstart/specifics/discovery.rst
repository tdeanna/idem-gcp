Controlling resource discovery
==============================

Most resources support discovery of resources even when the ``resource_id`` is not explicitly specified in the SLS.
What that means is, the GCP plugin will attempt to find a resource matching the specified properties, given that all identifying resource fields are provided.

For example, by default running ``idem state`` with the following SLS will try to find the bucket with name ``idem-bucket-123`` and if such already exists, ensure that it has the specified labels.
So in the case that the bucket exists, it would do an update operation if needed - even if the bucket has not been created with idem.

.. code-block:: sls

    idem-bucket:
      gcp.storage.bucket.present:
      - name: idem-bucket-123
      - labels:
          label-key: label-value


Output of ``idem state bucket.sls``:

.. code-block::

          ID: idem-bucket
    Function: gcp.storage.bucket.present
      Result: True
     Comment: ('Discovered existing gcp.storage.bucket with resource_id=b/idem-bucket-123', "gcp.storage.bucket 'idem-bucket-123' is up to date.")
     Changes:


This is the standard way idem works. However, there are cases where this is not desired behaviour.

Sometimes we may want to ensure that we are creating a new bucket and get an error if one already exists with this name,
instead of accidentally updating someone else's bucket.

This is where the ``--get-resource-only-with-resource-id`` flag comes in.

Running ``idem state`` with this flag ensures that the GCP plugin will update resources only when the user has explicitly
specified the ``resource_id`` or it is populated implicitly by the ESM cache.

Otherwise it will directly attempt a create operation and fail if the resource already exists.

Output of ``idem state bucket.sls --get-resource-only-with-resource-id``:

.. code-block::

          ID: idem-bucket
    Function: gcp.storage.bucket.present
      Result: False
     Comment: ("gcp.storage.bucket 'idem-bucket-123' already exists.",)
     Changes:


However, if you explicitly specify the ``resource_id`` in the SLS like so:

.. code-block:: sls

    idem-bucket:
      gcp.storage.bucket.present:
      - name: idem-bucket-123
      - resource_id: b/idem-bucket-123
      - labels:
          label-key: label-value

Then idem knows you want to do an update and the output of ``idem state bucket.sls --get-resource-only-with-resource-id`` is:

.. code-block::

          ID: idem-bucket
    Function: gcp.storage.bucket.present
      Result: True
     Comment: ("gcp.storage.bucket 'idem-bucket-123' is up to date.",)
     Changes:

Note: This flag does affect the idempotent semantics of idem, as the same SLS could result in different outcomes when ran twice - first time successfully creating the resource and next time failing to create.
So use this with caution.

You can find more about this functionality in the official idem developer docs: `Control Get Existing Resource With Id <https://docs.idemproject.io/idem/en/latest/developer/tutorials/create-plugin/get_resource_only_with_resource_id.html>`_

It was mentioned that the ESM cache could affect the presence of some properties like ``resource_id`` passed to the plugin.
If you'd like only the properties specified in the SLS to be passed to the plugin without any dependencies on prior ``idem state`` run results, the ESM cache could be disabled for the particular ``idem`` run using the ``--esm-plugin=null`` option.

``idem state bucket.sls --esm-plugin=null``

You could use both options at the same time to combine both functionalities and have full control over whether the Idem GCP plugin would create or update resources:

``idem state bucket.sls --get-resource-only-with-resource-id --esm-plugin=null``

This would:
 * Update the resource if ``resource_id`` is specified in the SLS, fail if resource doesn't already exist.
 * Create the resource if ``resource_id`` is not specified in the SLS, fail if resource already exists.

For more information about ESM cache, refer to the idem docs at `Enforced State Management <https://docs.idemproject.io/idem/en/latest/topics/explanation/enforced_state_management.html>`_
