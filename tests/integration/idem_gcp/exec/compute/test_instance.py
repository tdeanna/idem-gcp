from time import sleep

import pytest
import pytest_asyncio
from pytest_idem import runner

from tests.utils import generate_unique_name
from tests.utils import stop_instance
from tests.utils import wait_for_operation

PARAMETER = {
    "instance_name": generate_unique_name("idem-test-instance"),
    "disk_name": generate_unique_name("idem-test-disk"),
    "disk2_name": generate_unique_name("idem-test-disk"),
}

DISK_SPEC = f"""
{PARAMETER["disk_name"]}:
  gcp.compute.disk.present:
  - zone: us-central1-a
  - project: tango-gcp
  - type_: projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced
  - size_gb: '10'
  - guest_os_features:
      - type_: UEFI_COMPATIBLE
  - resource_policies:
    - projects/tango-gcp/regions/us-central1/resourcePolicies/default-schedule-1
"""

DISK2_SPEC = f"""
{PARAMETER["disk2_name"]}:
  gcp.compute.disk.present:
  - zone: us-central1-a
  - project: tango-gcp
  - type_: projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced
  - size_gb: '11'
  - resource_policies:
    - projects/tango-gcp/regions/us-central1/resourcePolicies/default-schedule-1
"""

ATTACH_DISK_SPEC = """
attach_disk_{device_name}:
  exec.run:
    - path: gcp.compute.instance.attach_disk
    - kwargs:
        resource_id: {resource_id}
        auto_delete: {auto_delete}
        device_name: {device_name}
        source: {source}
        disk_size_gb: {disk_size_gb}
        type_: {type_}
"""

DETACH_DISK_SPEC = """
attach_disk_{device_name}:
  exec.run:
    - path: gcp.compute.instance.detach_disk
    - kwargs:
        resource_id: {resource_id}
        device_name: {device_name}
"""

INSTANCE_SPEC = """
{name}:
  gcp.compute.instance.present:
  - name: {name}
  - project: tango-gcp
  - machine_type: projects/tango-gcp/zones/us-central1-a/machineTypes/f1-micro
  - zone: us-central1-a
  - can_ip_forward: false
  - network_interfaces:
    - access_configs:
      - kind: compute#accessConfig
        name: External NAT
        network_tier: PREMIUM
        set_public_ptr: false
        type_: ONE_TO_ONE_NAT
      kind: compute#networkInterface
      name: nic0
      network: projects/tango-gcp/global/networks/default
      stack_type: IPV4_ONLY
      subnetwork: projects/tango-gcp/regions/us-central1/subnetworks/default
  - disks:
    - auto_delete: true
      boot: true
      device_name: {disk}
      source: projects/tango-gcp/zones/us-central1-a/disks/{disk}
      mode: READ_WRITE
      type_: PERSISTENT
      guest_os_features:
      - type_: UEFI_COMPATIBLE
  - scheduling:
      automatic_restart: true
      on_host_maintenance: MIGRATE
      preemptible: false
      provisioning_model: STANDARD
  - deletion_protection: false
  - shielded_instance_config:
      enable_integrity_monitoring: false
      enable_secure_boot: false
      enable_vtpm: false
"""

INSTANCE2_SPEC = """
{name}:
  gcp.compute.instance.present:
    - can_ip_forward: false
    - confidential_instance_config:
      enable_confidential_compute: false
    - deletion_protection: false
    - description: ''
    - disks:
      - auto_delete: true
        boot: true
        device_name: {disk_name}
        initialize_params:
          disk_size_gb: '10'
          disk_type: projects/tango-gcp/zones/us-central1-a/diskTypes/pd-balanced
          source_image: projects/debian-cloud/global/images/debian-11-bullseye-v20221206
        mode: READ_WRITE
        type_: PERSISTENT
    - display_device:
      enable_display: false
    - guest_accelerators: []
    - key_revocation_action_type: NONE
    - machine_type: projects/tango-gcp/zones/us-central1-a/machineTypes/f1-micro
    - metadata:
      items: []
    - name: {name}
    - network_interfaces:
      - access_configs:
          - name: External NAT
            network_tier: PREMIUM
        stack_type: IPV4_ONLY
        subnetwork: projects/tango-gcp/regions/us-central1/subnetworks/default
    - reservation_affinity:
      consume_reservation_type: ANY_RESERVATION
    - scheduling:
      automatic_restart: false
      on_host_maintenance: MIGRATE
      provisioning_model: STANDARD
    - shielded_instance_config:
      enable_integrity_monitoring: true
      enable_secure_boot: false
      enable_vtpm: true
    - tags:
      items: []
    - zone: us-central1-a
"""


@pytest_asyncio.fixture(scope="function")
def test_disk(hub, idem_cli):
    global PARAMETER
    if "test_disk" in PARAMETER:
        return PARAMETER["test_disk"]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(DISK_SPEC)
        state_ret = idem_cli("state", fh, check=True).json
        ret = hub.tool.gcp.utils.get_esm_tagged_data(state_ret, "gcp.compute.disk")
        assert ret["result"], ret["comment"]
        new_resource = ret["new_state"]
        PARAMETER["test_disk"] = new_resource
        return new_resource


@pytest_asyncio.fixture(scope="function")
def test_disk_to_attach(hub, idem_cli):
    global PARAMETER
    if "test_disk_to_attach" in PARAMETER:
        return PARAMETER["test_disk_to_attach"]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(DISK2_SPEC)
        state_ret = idem_cli("state", fh, check=True).json
        ret = hub.tool.gcp.utils.get_esm_tagged_data(state_ret, "gcp.compute.disk")
        assert ret["result"], ret["comment"]
        new_resource = ret["new_state"]
        PARAMETER["test_disk_to_attach"] = new_resource
        return new_resource


@pytest_asyncio.fixture(scope="function")
def test_instance(hub, idem_cli, test_disk):
    global PARAMETER
    if "test_instance" in PARAMETER:
        return PARAMETER["test_instance"]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(
            INSTANCE_SPEC.format(
                **{
                    "name": PARAMETER["instance_name"],
                    "disk": PARAMETER["disk_name"],
                }
            )
        )
        state_ret = idem_cli("state", fh, check=True).json
        ret = hub.tool.gcp.utils.get_esm_tagged_data(state_ret, "gcp.compute.instance")
        assert ret["result"], ret["comment"]
        new_resource = ret["new_state"]
        PARAMETER["resource_id"] = new_resource.get("resource_id", "")
        PARAMETER["test_instance"] = new_resource
        return new_resource


@pytest.mark.asyncio
async def test_instance_list_filter(hub, ctx, test_instance):
    test_instance_name = test_instance["name"]
    ret = await hub.exec.gcp.compute.instance.list(
        ctx,
        zone=test_instance["zone"],
        filter_=f"name eq {test_instance_name}",
    )
    assert len(ret["ret"]) == 1
    instance = ret["ret"][0]
    assert hub.tool.gcp.resource_prop_utils.resource_type_matches(
        instance["resource_id"], "compute.instance"
    )
    assert instance["name"] == test_instance_name

    ret = await hub.exec.gcp.compute.instance.list(
        ctx,
        filter_=f"name ne {test_instance_name}",
    )
    for instance in ret["ret"]:
        assert hub.tool.gcp.resource_prop_utils.resource_type_matches(
            instance["resource_id"], "compute.instance"
        )
        assert instance["name"] != test_instance_name


@pytest.mark.asyncio
async def test_instance_get_by_resource_id(hub, ctx, test_instance):
    ret = await hub.exec.gcp.compute.instance.get(
        ctx, resource_id=test_instance["resource_id"]
    )
    instance = ret["ret"]
    assert hub.tool.gcp.resource_prop_utils.resource_type_matches(
        instance["resource_id"], "compute.instance"
    )
    assert instance["name"] == test_instance["name"]

    ret = await hub.exec.gcp.compute.instance.get(
        ctx,
        resource_id="@@##$$%%^^",
    )
    assert not ret["ret"]


@pytest.mark.asyncio
async def test_instance_get_by_project_zone_and_name(hub, ctx, test_instance):
    ret = await hub.exec.gcp.compute.instance.get(
        ctx,
        zone=test_instance["zone"],
        name=test_instance["name"],
    )
    instance = ret["ret"]
    assert hub.tool.gcp.resource_prop_utils.resource_type_matches(
        instance["resource_id"], "compute.instance"
    )
    assert instance["name"] == test_instance["name"]

    ret = await hub.exec.gcp.compute.instance.get(
        ctx,
        name="@@##$$%%^^",
    )
    assert not ret["ret"]


@pytest.mark.asyncio
async def test_instance_set_disk_auto_delete(hub, ctx, test_instance):
    flag = test_instance["disks"][0]["auto_delete"]
    ret = await hub.exec.gcp.compute.instance.set_disk_auto_delete(
        ctx,
        zone=test_instance["zone"],
        instance=test_instance["name"],
        device_name=test_instance["disks"][0]["device_name"],
        auto_delete=not flag,
    )
    instance = ret["ret"]
    assert instance
    assert (not flag) == instance["disks"][0]["auto_delete"]

    ret = await hub.exec.gcp.compute.instance.set_disk_auto_delete(
        ctx,
        zone=test_instance["zone"],
        instance=test_instance["name"],
        device_name=test_instance["disks"][0]["device_name"],
        auto_delete=flag,
    )
    instance = ret["ret"]
    assert instance
    assert flag == instance["disks"][0]["auto_delete"]


@pytest.mark.asyncio
async def test_instance_attach_disk(
    hub, ctx, idem_cli, test_instance, test_disk_to_attach
):
    # Initially, test instance has only one boot disk
    assert len(test_instance["disks"]) == 1
    assert test_instance["disks"][0]["device_name"] == PARAMETER["disk_name"]

    params = {
        "resource_id": test_instance.get("resource_id"),
        "source": test_disk_to_attach.get("resource_id"),
        "disk_size_gb": test_disk_to_attach.get("size_gb"),
        "type_": test_disk_to_attach.get("type_"),
        "device_name": test_disk_to_attach.get("name"),
        "auto_delete": False,
    }

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(ATTACH_DISK_SPEC.format(**params))
        state_ret = idem_cli("state", fh, check=True).json
        ret = next(
            iter(
                [_ for key, _ in state_ret.items() if f'{params["device_name"]}' in key]
            )
        )

        assert ret["result"], ret["comment"]
        instance = ret["new_state"]

        assert instance
        assert len(instance["disks"]) == 2
        assert instance["disks"][0]["device_name"] == PARAMETER["disk_name"]
        assert instance["disks"][1]["device_name"] == PARAMETER["disk2_name"]

    with runner.named_tempfile(suffix=".sls") as fh:
        fh.write_text(DETACH_DISK_SPEC.format(**params))
        state_ret = idem_cli("state", fh, check=True).json
        ret = next(
            iter(
                [_ for key, _ in state_ret.items() if f'{params["device_name"]}' in key]
            )
        )
        assert ret["result"], ret["comment"]
        instance = ret["new_state"]

        assert instance
        assert len(instance["disks"]) == 1
        assert instance["disks"][0]["device_name"] == PARAMETER["disk_name"]


@pytest.mark.asyncio
async def test_instance_get_effective_firewalls(hub, ctx, idem_cli, test_instance):
    # The network interface of the network with name 'default' which is specified in the test_instance
    network_interface = test_instance["network_interfaces"][0]["name"]

    ret = await hub.exec.gcp.compute.instance.get_effective_firewalls(
        ctx,
        resource_id=test_instance["resource_id"],
        network_interface=network_interface,
    )

    # The 'default' network has 3 firewalls, http, https, ssh. At least one should be returned (ssh),
    # Maybe http and https as well if the traffic is enabled in the specified instance
    assert ret["ret"], ret["result"]
    assert not ret["comment"]


@pytest.mark.asyncio
async def test_instance_update_shielded_instance_config(hub, ctx, test_instance):
    config = test_instance["shielded_instance_config"]
    assert not config["enable_integrity_monitoring"]
    assert not config["enable_secure_boot"]
    assert not config["enable_vtpm"]

    zone = test_instance["zone"]
    instance = test_instance["name"]

    stop_op = stop_instance(ctx, ctx.acct.project_id, zone, instance)
    wait_for_operation(ctx, ctx.acct.project_id, zone, stop_op)

    ret = await hub.exec.gcp.compute.instance.update_shielded_instance_config(
        ctx,
        False,
        True,
        True,
        zone=zone,
        instance=instance,
    )
    instance = ret["ret"]

    assert instance
    assert instance["shielded_instance_config"]["enable_vtpm"]
    assert instance["shielded_instance_config"]["enable_integrity_monitoring"]
    assert not instance["shielded_instance_config"]["enable_secure_boot"]


@pytest.mark.skip(reason="Flaky test.")
async def test_instance_lifecycle(hub, ctx, idem_cli):
    # TERMINATED ---.start()---> RUNNING ---.suspend()---> SUSPENDED ---.resume()---> RUNNING ---.stop()---> TERMINATED

    instance_name = generate_unique_name("idem-test-instance")
    disk_name = generate_unique_name("idem-test-disk")
    ret = hub.tool.utils.call_present_from_sls(
        idem_cli,
        INSTANCE2_SPEC.format(**{"name": instance_name, "disk_name": disk_name}),
    )
    assert ret["result"], ret["new_state"]
    resource_id = ret["new_state"].get("resource_id")

    while True:
        ret = await hub.exec.gcp.compute.instance.get(ctx, resource_id=resource_id)
        assert ret["ret"], ret["result"]
        instance = ret["ret"]
        if instance.get("status") in ["RUNNING", "SUSPENDED", "TERMINATED"]:
            break
        sleep(3)

    transitions = {
        ("TERMINATED", "RUNNING"): hub.exec.gcp.compute.instance.start,
        ("RUNNING", "TERMINATED"): hub.exec.gcp.compute.instance.stop,
        ("RUNNING", "SUSPENDED"): hub.exec.gcp.compute.instance.suspend,
        ("SUSPENDED", "RUNNING"): hub.exec.gcp.compute.instance.resume,
    }
    test_sequences = {
        "TERMINATED": ["RUNNING", "SUSPENDED", "RUNNING"],
        "SUSPENDED": ["RUNNING", "TERMINATED", "RUNNING"],
        "RUNNING": ["SUSPENDED", "RUNNING", "TERMINATED"],
    }

    current_state = instance.get("status")
    test_sequence = test_sequences[current_state]
    transition_failure = None
    for next_state in test_sequence:
        if transition_failure:
            break
        transition = transitions[(current_state, next_state)]
        hub.log.warning(
            f"Transitioning {resource_id} from {current_state} to {next_state} using {transition}"
        )
        sleep(3)
        ret = await transition(ctx, resource_id=resource_id)
        assert ret["ret"], ret["result"]

        iterations = 0
        while instance.get("status") != next_state:
            iterations += 1
            if iterations > 100:
                transition_failure = f"Transitioning {resource_id} from {current_state} to {next_state} using {transition} didn't complete on time"
                break
            sleep(3)
            ret = await hub.exec.gcp.compute.instance.get(ctx, resource_id=resource_id)
            assert ret["ret"], ret["result"]
            instance = ret["ret"]
            hub.log.warning(f"Current state of {resource_id} {instance.get('status')}")
        current_state = next_state

    ret = hub.tool.utils.call_absent(
        idem_cli, "compute.instance", instance_name, resource_id
    )

    assert not transition_failure
